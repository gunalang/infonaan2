package infonix.naan.live.network;


import java.util.Map;

import infonix.naan.live.model.CallListModel;
import infonix.naan.live.model.CallStats.CallStatsModel;
import infonix.naan.live.model.SubMenu;
import infonix.naan.live.model.menu.MenuDataModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface ApiService {

    @GET("menu.php?")
    @Headers("Accept: application/json")
    Call<MenuDataModel> getMenuList(@Query("uid") String uid);

    @GET
    @Headers("Accept: application/json")
    Call<MenuDataModel> getMethodSubMenuList(@Url String url);

    @POST
    @Headers("Accept: application/json")
    Call<MenuDataModel> postMethodSubMenuList(@Url String url, @QueryMap Map<String, String> params);

    @GET
    @Headers("Accept: application/json")
    Call<MenuDataModel> getMethodCallList(@Url String url);

    @POST
    @Headers("Accept: application/json")
    Call<MenuDataModel> postMethodCallList(@Url String url, @QueryMap Map<String, String> params);

    @GET
    @Headers("Accept: application/json")
    Call<CallStatsModel> getMethodCallStats(@Url String url);

    @POST
    @Headers("Accept: application/json")
    Call<CallStatsModel> postMthodCallStats(@Url String url, @QueryMap Map<String, String> params);

}
