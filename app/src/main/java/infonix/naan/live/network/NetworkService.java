package infonix.naan.live.network;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import infonix.naan.live.utils.GlobalData;

@SuppressWarnings("deprecation")
public class NetworkService {

	public static final int CONNECTION_TIMEOUT = 30 * 1000;
	public static final int SOCKET_TIMEOUT = 30 * 1000;
	public static final int SOCKET_BUFFER_SIZE = 8192;


	public String processWebRequest(String path,
									List<NameValuePair> nameValuePairs, boolean isAccessToken,
									boolean isPostMethod) {
		String mResponse = "";
		if (isPostMethod) {
			HttpResponse response;
			String url = GlobalData.BASE_URL + path;
			if (url.contains(" ")) {
				url = url.replace(" ", "%20");
			}
			try {
				HttpParams httpParams = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParams,
						CONNECTION_TIMEOUT);
				HttpConnectionParams.setSoTimeout(httpParams, SOCKET_TIMEOUT);

				HttpClient client = new DefaultHttpClient(httpParams);
				HttpPost httpPostRequest = new HttpPost(url);
				httpPostRequest.setHeader("Accept", "application/json");
				httpPostRequest.setHeader("Content-Type",
						"application/x-www-form-urlencoded");


				httpPostRequest.setEntity(new UrlEncodedFormEntity(
						nameValuePairs));

				response = client.execute(httpPostRequest);

				StatusLine statusLine = response.getStatusLine();

				if (statusLine.getStatusCode() == HttpStatus.SC_OK
						|| statusLine.getStatusCode() == HttpStatus.SC_CREATED) {
					HttpEntity entity = response.getEntity();
					InputStream is = entity.getContent();
					mResponse = convertStreamToString(is);
				} else if (statusLine.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
					HttpEntity entity = response.getEntity();
					InputStream is = entity.getContent();
					mResponse = convertStreamToString(is);
				} else {
					mResponse = "connectionFailure";
				}

			} catch (Exception e) {
				mResponse = "connectionFailure";
				e.printStackTrace();
			}

		} else {
			HttpClient httpclient = new DefaultHttpClient();
			HttpParams params = httpclient.getParams();
			HttpResponse response;
			String url = GlobalData.BASE_URL + path;
			if (url.contains(" ")) {
				url = url.replace(" ", "%20");
			}
			try {
				HttpConnectionParams.setConnectionTimeout(params,
						CONNECTION_TIMEOUT);
				HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);
				HttpConnectionParams.setSocketBufferSize(params,
						SOCKET_BUFFER_SIZE);
				HttpConnectionParams.setStaleCheckingEnabled(params, false);
				HttpProtocolParams.setContentCharset(params,
						HTTP.DEFAULT_CONTENT_CHARSET);
				HttpProtocolParams.setUseExpectContinue(params, true);
				HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
				HttpProtocolParams.setContentCharset(params, "UTF-8");

				HttpGet httpPost = new HttpGet(url);
				// httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				httpPost.setHeader("Accept", "application/json");


				httpPost.setHeader("Content-Type",
						"application/x-www-form-urlencoded");

				response = httpclient.execute(httpPost);
				StatusLine statusLine = response.getStatusLine();

				if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
					BufferedInputStream is = null;
					String result = "";
					HttpEntity httpEntity = response.getEntity();
					is = new BufferedInputStream(httpEntity.getContent());

					try {
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(is, "utf-8"), 8);
						StringBuilder sb = new StringBuilder();
						String line = null;
						while ((line = reader.readLine()) != null) {
							sb.append(line + "\n");
						}
						is.close();
						result = sb.toString();
						mResponse = result;
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else {
					mResponse = "connectionFailure";
				}
			} catch (Exception e) {
				mResponse = "connectionFailure";
			}
		}
		return mResponse;
	}

	public static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is),
				8192);
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append((line + "\n"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}
