package infonix.naan.live.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;

import infonix.naan.live.R;
import infonix.naan.live.model.menu.BlockContent;


public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    public interface HomeItemClickEventListener {

        void homeItemClickEvent(int position, int colorCode, String name, String linkid, int childBlockosition,
                                String layout, String count);

    }

    private ArrayList<BlockContent> menuModelArrayList;
    private Context context;
    private HomeItemClickEventListener homeItemClickEventListener;
    private int colorCode = Color.TRANSPARENT;
    private Drawable background;
    private boolean isVeritlcal;
    private int childBlockosition;
    private int layoutWidth;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, count;
        ImageView icon;
        LinearLayout frame;

        MyViewHolder(View view) {
            super(view);
            frame = view.findViewById(R.id.ll_frame);
            icon = view.findViewById(R.id.iv_icon);
            name = view.findViewById(R.id.tv_name);
            count = view.findViewById(R.id.tv_count);

            if (!isVeritlcal) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(layoutWidth, ViewGroup.LayoutParams.MATCH_PARENT);
                frame.setLayoutParams(params);
            }
        }
    }

    public HomeAdapter(int childBlockosition, Context context, ArrayList<BlockContent> arrayList,
                       HomeItemClickEventListener homeItemClickEventListener, boolean isVeritlcal, int layoutWidth) {
        this.menuModelArrayList = arrayList;
        this.context = context;
        this.homeItemClickEventListener = homeItemClickEventListener;
        this.isVeritlcal = isVeritlcal;
        this.layoutWidth = layoutWidth;
        this.childBlockosition = childBlockosition;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;
        if (isVeritlcal)
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.vertical_home_item, parent, false);
        else
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.horizontal_home_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final BlockContent menuModel = menuModelArrayList.get(position);

        if (!isVeritlcal) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(layoutWidth, ViewGroup.LayoutParams.MATCH_PARENT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(5);
                if (position == menuModelArrayList.size() - 1)
                    params.setMarginEnd(5);
            }
            holder.frame.setLayoutParams(params);
        }
        holder.frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                background = view.getBackground();
                if (background instanceof ColorDrawable)
                    colorCode = ((ColorDrawable) background).getColor();
                homeItemClickEventListener.homeItemClickEvent(holder.getAdapterPosition(), colorCode,
                        menuModel.getLinkName(), menuModel.getLinkId(), childBlockosition,
                        menuModel.getLinkSettings().getLayout(), menuModel.getContCount());
            }
        });
        if (menuModel.getLinkName() != null && !menuModel.getLinkName().isEmpty()) {
            holder.name.setText(menuModel.getLinkName());
        } else {
            holder.name.setText("-");
        }
        if (menuModel.getContCount() != null && !menuModel.getContCount().isEmpty()) {
            holder.count.setText(menuModel.getContCount());
            holder.count.setVisibility(View.VISIBLE);
        } else {
            holder.count.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return menuModelArrayList.size();
    }
}
