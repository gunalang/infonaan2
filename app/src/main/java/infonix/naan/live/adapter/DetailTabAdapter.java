package infonix.naan.live.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import infonix.naan.live.fragment.CurrentEventFragment;
import infonix.naan.live.fragment.EventDetailFragment;
import infonix.naan.live.fragment.TimelineFragment;


public class DetailTabAdapter extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public DetailTabAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                TimelineFragment tab1 = new TimelineFragment();
                return tab1;
            case 1:
                CurrentEventFragment tab2 = new CurrentEventFragment();
                return tab2;
            case 2:
                EventDetailFragment tab3 = new EventDetailFragment();
                return tab3;
            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Timeline";
            case 1:
                return "Current Event";
            case 2:
                return "Detail";
            default:
                return "Detail";
        }
    }
}
