package infonix.naan.live.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import butterknife.BindView;
import butterknife.ButterKnife;
import infonix.naan.live.R;
import infonix.naan.live.view.stepperview.TimelineView;


public class TimeLineViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text_title)
    TextView title;
    @BindView(R.id.text_calls)
    TextView calls;
    @BindView(R.id.text_days)
    TextView days;
    @BindView(R.id.time_marker)
    TimelineView mTimelineView;
    @BindView(R.id.cardview_text)
    CardView cardView;

    public TimeLineViewHolder(View itemView, int viewType) {
        super(itemView);

        ButterKnife.bind(this, itemView);
        mTimelineView.initLine(viewType);
    }
}
