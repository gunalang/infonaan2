package infonix.naan.live.adapter;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import infonix.naan.live.R;
import infonix.naan.live.activity.HomeDetailActivity;
import infonix.naan.live.listener.ObservableObject;
import infonix.naan.live.model.CallRecordListModel;
import infonix.naan.live.utils.DateTimeUtils;
import infonix.naan.live.view.expandablelayout.ExpandableLayout;
import infonix.naan.live.view.jcplayer.JcAudio;
import infonix.naan.live.view.jcplayer.JcPlayerView;


public class CallRecordAdapter extends RecyclerView.Adapter<CallRecordAdapter.ViewHolder> implements Observer {

    @Override
    public void update(Observable observable, Object o) {
        if (o.toString().equals("stopMediaPlayer")) {
            // if (player != null && player.isPlaying())
            //   player.kill();
        }
    }

    public interface AdapterInterface {

        void deleteandset(int position);

    }

    LayoutInflater inflater;

    public Context context;
    public AdapterInterface adapterInterface;
    private ArrayList<CallRecordListModel> callListModelArrayList = new ArrayList<CallRecordListModel>();
    private static final int UNSELECTED = -1;
    private int selectedItem = UNSELECTED;
    private RecyclerView recyclerView;

    Handler handler = new Handler();

    public CallRecordAdapter(LayoutInflater inflater1, ArrayList<CallRecordListModel> callListModelArrayList,
                             Context context, AdapterInterface adapterInterface,
                             RecyclerView recyclerView) {

        try {
            inflater = inflater1;
            this.context = context;
            this.callListModelArrayList = callListModelArrayList;
            this.adapterInterface = adapterInterface;
            this.recyclerView = recyclerView;
            ObservableObject.getInstance().addObserver(this);
        } catch (Exception e) {
            Log.e("intilisation adapter", "exception");
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.callrecord_item,
                parent, false);

        return new ViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(@NonNull final CallRecordAdapter.ViewHolder holder, int position) {

        holder.bind(position);
        holder.delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (context != null)
                    new android.support.v7.app.AlertDialog.Builder(context)
                            .setMessage("Ar you sure to delete this file?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        adapterInterface.deleteandset(holder.getAdapterPosition());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return callListModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            ExpandableLayout.OnExpansionUpdateListener {

        View itemView;
        TextView name, time, date, duration;
        ImageView calltype, delete;
        CheckBox play;
        ImageButton opt;
        RelativeLayout rl;
        private JcPlayerView player;
        ExpandableLayout expandableLayout;
        int position;

        ViewHolder(View convertView) {
            super(convertView);
            this.itemView = itemView;
            rl = convertView.findViewById(R.id.expandable_toggle_button);
            expandableLayout = convertView.findViewById(R.id.expandable_seekbar);
            player = convertView.findViewById(R.id.jcplayer);
            name = convertView.findViewById(R.id.name);
            time = convertView.findViewById(R.id.time);
            duration = convertView.findViewById(R.id.duration);
            date = convertView.findViewById(R.id.date);
            calltype = convertView.findViewById(R.id.calltype);
            delete = convertView.findViewById(R.id.iv_delete);
            play = convertView.findViewById(R.id.iv_play);

            expandableLayout.setInterpolator(new LinearInterpolator());
            expandableLayout.setOnExpansionUpdateListener(this);
            play.setOnClickListener(this);
        }

        public void bind(int pos) {
            this.position = pos;

            //Setting image according to received, dialled or missed
            if (callListModelArrayList.get(position).getCallType() != null) {
                switch (callListModelArrayList.get(position).getCallType()) {
                    case "incoming":
                        calltype.setBackgroundResource(R.drawable.sym_call_incoming);
                        break;
                    case "outgoing":
                        calltype.setBackgroundResource(R.drawable.sym_call_outgoing);
                        break;
                    case "missed":
                        calltype.setBackgroundResource(R.drawable.sym_call_missed);
                        break;
                }
            }
            if (callListModelArrayList.get(position).getStatus() != null) {
                if (callListModelArrayList.get(position).getStatus().equals("true")) {
                    rl.setBackgroundColor(context.getResources().getColor(R.color.light_yellow));
                } else {
                    rl.setBackgroundColor(context.getResources().getColor(R.color.light_white));
                }
            }
            String sname = null;
            try {
                sname = getContactName(callListModelArrayList.get(position).getNumber());
            } catch (Exception e) {
                Log.e("error", e.toString());
            }
            if (sname != null) {
                name.setText(sname);
            } else
                name.setText(callListModelArrayList.get(position).getNumber());

            if (callListModelArrayList.get(position).getTime() != null)
                time.setText(callListModelArrayList.get(position).getTime());

            if (callListModelArrayList.get(position).getDuration() != null)
                duration.setText(callListModelArrayList.get(position).getDuration());

            play.setSelected(false);
            expandableLayout.collapse(false);
        }

        @Override
        public void onClick(View view) {
            ViewHolder holder = (ViewHolder) recyclerView.findViewHolderForAdapterPosition(selectedItem);
            if (holder != null) {
                holder.play.setSelected(false);
                holder.play.setChecked(false);
                holder.expandableLayout.collapse();
                if (player != null && player.isPlaying())
                    player.kill();
            }

            if (position == selectedItem) {
                selectedItem = UNSELECTED;
            } else {
                play.setSelected(true);
                expandableLayout.expand();
                selectedItem = position;
                List<JcAudio> jcAudios = new ArrayList<>();
                jcAudios.add(JcAudio.createFromFilePath(callListModelArrayList.get(position).getNumber(),
                        callListModelArrayList.get(position).getFilePath()));
                player.initPlaylist(jcAudios);
                player.playAudio(player.getMyPlaylist().get(0));
            }
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            if (getAdapterPosition() >= 0 && getAdapterPosition() < callListModelArrayList.size())
                recyclerView.smoothScrollToPosition(getAdapterPosition());
        }
    }

    public Uri getContactPhoto(String phoneNumber) throws Exception {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = context.getContentResolver().query(uri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID},
                null, null, null);

        long contactId = 0;

        if (cursor.moveToFirst()) {
            do {
                contactId = cursor.getLong(cursor
                        .getColumnIndex(ContactsContract.PhoneLookup._ID));
            } while (cursor.moveToNext());
        }

        return getUserPictureUri(contactId);

    }

    private Uri getUserPictureUri(long id) {
        Uri person = ContentUris.withAppendedId(
                ContactsContract.Contacts.CONTENT_URI, id);

        Uri picUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
        try {
            InputStream is = ContactsContract.Contacts.openContactPhotoInputStream(
                    context.getContentResolver(), picUri);
            is.close();
        } catch (FileNotFoundException e) {
            //Contact image does not exist
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            //Log.d(" picture exception", "called");
            return null;
        }

        return picUri;
    }

    private String getContactName(String snumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(snumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }

        return contactName;
    }
}
