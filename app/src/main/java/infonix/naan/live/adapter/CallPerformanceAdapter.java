package infonix.naan.live.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.List;

import infonix.naan.live.R;
import infonix.naan.live.model.CallStats.CallPerformance;
import infonix.naan.live.model.OrderStatus;
import infonix.naan.live.model.TimeLineModel;
import infonix.naan.live.utils.DateTimeUtils;
import infonix.naan.live.utils.VectorDrawableUtils;
import infonix.naan.live.view.stepperview.TimelineView;


public class CallPerformanceAdapter extends RecyclerView.Adapter<TimeLineViewHolder> {

    private List<CallPerformance> callPerformanceList;
    private Context mContext;
    private boolean mWithLinePadding;
    private LayoutInflater mLayoutInflater;


    public CallPerformanceAdapter(List<CallPerformance> feedList, boolean withLinePadding) {
        this.callPerformanceList = feedList;
        this.mWithLinePadding = withLinePadding;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    @NonNull
    @Override
    public TimeLineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        view = mLayoutInflater.inflate(R.layout.item_timeline, parent, false);

        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeLineViewHolder holder, int position) {

        CallPerformance callPerformance = callPerformanceList.get(position);

        if (callPerformance.getCallPositionName() != null) {
            holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext,
                    R.drawable.timeline_marker), ContextCompat.getColor(mContext, R.color.colorPrimary));
            holder.title.setTextColor(Color.BLACK);
            holder.calls.setTextColor(Color.GRAY);
            holder.days.setTextColor(Color.GRAY);
            /*holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext,
                    R.drawable.timeline_marker_inactive, android.R.color.darker_gray));
            holder.mDate.setTextColor(Color.GRAY);
            holder.mMessage.setTextColor(Color.BLACK);*/
        } else {
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext,
                    R.drawable.timeline_marker_active, R.color.colorPrimary));
            holder.title.setTextColor(Color.BLUE);
            holder.calls.setTextColor(Color.GRAY);
            holder.days.setTextColor(Color.GRAY);
        }/* else {
            holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext,
                    R.drawable.timeline_marker), ContextCompat.getColor(mContext, R.color.colorPrimary));
            holder.mDate.setTextColor(Color.GRAY);
            holder.mMessage.setTextColor(Color.BLACK);
        }*/
        if (callPerformance.getCallPositionName() != null && !callPerformance.getCallPositionName().isEmpty()) {
            holder.title.setText(callPerformance.getCallPositionName());
        } else
            holder.title.setText("");
        if (callPerformance.getNumberOfCalls() != null && !callPerformance.getNumberOfCalls().isEmpty())
            holder.calls.setText("(" + callPerformance.getNumberOfCalls() + ")");
        else
            holder.calls.setText("");
        if (callPerformance.getNumberofDays() != null && !callPerformance.getNumberofDays().isEmpty()) {
            holder.days.setText("(" + callPerformance.getNumberofDays() + ")");
        } else
            holder.days.setText("");
    }

    @Override
    public int getItemCount() {
        return (callPerformanceList != null ? callPerformanceList.size() : 0);
    }

}
