package infonix.naan.live.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Random;

import infonix.naan.live.R;
import infonix.naan.live.activity.HomeDetailActivity;
import infonix.naan.live.model.CallListItemModel;
import infonix.naan.live.model.menu.ArgData;
import infonix.naan.live.model.menu.BlockContent;
import infonix.naan.live.utils.UrlImageParser;


public class HomeDetailAdapter extends RecyclerView.Adapter<HomeDetailAdapter.MyViewHolder> {

    public interface GroupitemClickListener {

        void groupitemClickEvent(int position, String targetLocationId, String linkName, String linkId, String layout,
                                 String url, String method, ArrayList<ArgData> argDataArrayList);

        void callOptionClickEvent(String linkName);

    }

    String html = "<span style=\"background-color:#77BA09;\"><font color=red> sample text</font></span>";


    private ArrayList<BlockContent> callListModelArrayListItem;
    private WeakReference<HomeDetailActivity> context;
    private Context cxt;
    private GroupitemClickListener groupitemClickListener;
    private int groupId;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        TextView clientType;
        ImageView pic;
        LinearLayout frame;
        FrameLayout circleLayout;
        TextView header;
        TextView callPosition;
        TextView agecount, html;
        ImageView calllist;

        MyViewHolder(View view) {
            super(view);
            frame = view.findViewById(R.id.card_frame);
            name = view.findViewById(R.id.tv_name);
            html = view.findViewById(R.id.html);
            header = view.findViewById(R.id.tv_title);
            agecount = view.findViewById(R.id.tv_agecount);
            callPosition = view.findViewById(R.id.tv_callpos);
            clientType = view.findViewById(R.id.tv_clienttype);
            pic = view.findViewById(R.id.iv_image);
            calllist = view.findViewById(R.id.iv_calllist);
            circleLayout = view.findViewById(R.id.view);

            calllist.setVisibility(View.GONE);
        }
    }

    public HomeDetailAdapter(WeakReference<HomeDetailActivity> context, ArrayList<BlockContent> callListModelArrayListItem,
                             GroupitemClickListener groupitemClickListener, int groupId, Context cxt) {
        this.callListModelArrayListItem = callListModelArrayListItem;
        this.context = context;
        this.groupitemClickListener = groupitemClickListener;
        this.groupId = groupId;
        this.cxt = cxt;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_detail_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        Random rnd = new Random();
        int color = Color.argb(200, rnd.nextInt(200), rnd.nextInt(200), rnd.nextInt(200));
        //  holder.frame.setBackgroundColor(color);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.circleLayout.setBackgroundTintList(ColorStateList.valueOf(color));
        } else {
            holder.circleLayout.setBackgroundColor(color);
        }

        holder.calllist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                groupitemClickListener.callOptionClickEvent(callListModelArrayListItem.get(holder.getAdapterPosition()).getLinkName());
            }
        });

        holder.frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                groupitemClickListener.groupitemClickEvent(holder.getAdapterPosition(),
                        callListModelArrayListItem.get(position).getLinkSettings().getTargetLocationId(),
                        callListModelArrayListItem.get(position).getLinkSettings().getScreenName(),
                        callListModelArrayListItem.get(position).getLinkSettings().getScreenName(),
                        callListModelArrayListItem.get(position).getLinkSettings().getLayout(),
                        callListModelArrayListItem.get(position).getLinkSettings().getFetchURL(),
                        callListModelArrayListItem.get(position).getLinkSettings().getArgType(),
                        callListModelArrayListItem.get(position).getLinkSettings().getArgData());
            }
        });

        UrlImageParser p = new UrlImageParser(holder.html, cxt);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (callListModelArrayListItem.get(position).getLinkExtraContent().getHtml() != null) {
                Spanned htmlSpan = Html.fromHtml(callListModelArrayListItem.get(position).getLinkExtraContent().getHtml(),
                        Html.FROM_HTML_MODE_LEGACY, p, null);
                holder.html.setText(htmlSpan);
            }
        } else {
            if (callListModelArrayListItem.get(position).getLinkExtraContent().getHtml() != null) {
                Spanned htmlSpan = Html.fromHtml(
                        callListModelArrayListItem.get(position).getLinkExtraContent().getHtml(), p, null);
                holder.html.setText(htmlSpan);
            }
        }

        if (groupId == 0) {
            if (callListModelArrayListItem.get(position).getLinkTitle() != null &&
                    !callListModelArrayListItem.get(position).getLinkTitle().isEmpty()) {
                holder.name.setText(firstUpperCase(callListModelArrayListItem.get(position).getLinkTitle()));
            } else {
                holder.name.setText("-");
            }
            if (callListModelArrayListItem.get(position).getLinkExtraContent().getClientType() != null &&
                    !callListModelArrayListItem.get(position).getLinkExtraContent().getClientType().isEmpty()) {
                holder.clientType.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getClientType());
            } else {
                holder.clientType.setText("-");
            }
            if (callListModelArrayListItem.get(position).getLinkExtraContent().getCallPosition() != null &&
                    !callListModelArrayListItem.get(position).getLinkExtraContent().getCallPosition().isEmpty()) {
                holder.callPosition.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getCallPosition());
            } else {
                holder.callPosition.setText("-");
            }
            if (callListModelArrayListItem.get(position).getLinkExtraContent().getAgeCount() != null &&
                    !callListModelArrayListItem.get(position).getLinkExtraContent().getAgeCount().isEmpty()) {
                holder.agecount.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getAgeCount());
            } else {
                holder.agecount.setText("-");
            }

            if (holder.getAdapterPosition() > 0) {
                if (callListModelArrayListItem.get(position).getLinkExtraContent().getNextCallDate().equalsIgnoreCase(
                        callListModelArrayListItem.get(position - 1).getLinkExtraContent().getNextCallDate())) {
                    holder.header.setVisibility(View.GONE);
                } else {
                    holder.header.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getNextCallDate());
                    holder.header.setVisibility(View.VISIBLE);
                }
            } else {
                holder.header.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getNextCallDate());
                holder.header.setVisibility(View.VISIBLE);
            }
        } else if (groupId == 1) {
            if (callListModelArrayListItem.get(position).getLinkTitle() != null &&
                    !callListModelArrayListItem.get(position).getLinkTitle().isEmpty()) {
                holder.name.setText(firstUpperCase(callListModelArrayListItem.get(position).getLinkTitle()));
            } else {
                holder.name.setText("-");
            }
            if (callListModelArrayListItem.get(position).getLinkExtraContent().getClientType() != null &&
                    !callListModelArrayListItem.get(position).getLinkExtraContent().getClientType().isEmpty()) {
                holder.clientType.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getClientType());
            } else {
                holder.clientType.setText("-");
            }
            if (callListModelArrayListItem.get(position).getLinkExtraContent().getNextCallDate() != null &&
                    !callListModelArrayListItem.get(position).getLinkExtraContent().getNextCallDate().isEmpty()) {
                holder.callPosition.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getNextCallDate());
            } else {
                holder.callPosition.setText("-");
            }
            if (callListModelArrayListItem.get(position).getLinkExtraContent().getAgeCount() != null &&
                    !callListModelArrayListItem.get(position).getLinkExtraContent().getAgeCount().isEmpty()) {
                holder.agecount.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getAgeCount());
            } else {
                holder.agecount.setText("-");
            }

            if (holder.getAdapterPosition() > 0) {
                if (callListModelArrayListItem.get(position).getLinkExtraContent().getCallPosition().equalsIgnoreCase(
                        callListModelArrayListItem.get(position - 1).getLinkExtraContent().getCallPosition())) {
                    holder.header.setVisibility(View.GONE);
                } else {
                    holder.header.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getCallPosition());
                    holder.header.setVisibility(View.VISIBLE);
                }
            } else {
                holder.header.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getCallPosition());
                holder.header.setVisibility(View.VISIBLE);
            }
        } else if (groupId == 2) {
            if (callListModelArrayListItem.get(position).getLinkTitle() != null &&
                    !callListModelArrayListItem.get(position).getLinkTitle().isEmpty()) {
                holder.name.setText(firstUpperCase(callListModelArrayListItem.get(position).getLinkTitle()));
            } else {
                holder.name.setText("-");
            }
            if (callListModelArrayListItem.get(position).getLinkExtraContent().getNextCallDate() != null &&
                    !callListModelArrayListItem.get(position).getLinkExtraContent().getNextCallDate().isEmpty()) {
                holder.clientType.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getNextCallDate());
            } else {
                holder.clientType.setText("-");
            }
            if (callListModelArrayListItem.get(position).getLinkExtraContent().getCallPosition() != null &&
                    !callListModelArrayListItem.get(position).getLinkExtraContent().getCallPosition().isEmpty()) {
                holder.callPosition.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getCallPosition());
            } else {
                holder.callPosition.setText("-");
            }
            if (callListModelArrayListItem.get(position).getLinkExtraContent().getAgeCount() != null &&
                    !callListModelArrayListItem.get(position).getLinkExtraContent().getAgeCount().isEmpty()) {
                holder.agecount.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getAgeCount());
            } else {
                holder.agecount.setText("-");
            }

            if (holder.getAdapterPosition() > 0) {
                if (callListModelArrayListItem.get(position).getLinkExtraContent().getClientType().equalsIgnoreCase(
                        callListModelArrayListItem.get(position - 1).getLinkExtraContent().getClientType())) {
                    holder.header.setVisibility(View.GONE);
                } else {
                    holder.header.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getClientType());
                    holder.header.setVisibility(View.VISIBLE);
                }
            } else {
                holder.header.setText(callListModelArrayListItem.get(position).getLinkExtraContent().getClientType());
                holder.header.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public int getItemCount() {
        return callListModelArrayListItem.size();
    }

    private String firstUpperCase(String value) {
        if (value != null && value.length() > 0) {
            StringBuilder rackingSystemSb = new StringBuilder(value);
            rackingSystemSb.setCharAt(0, Character.toUpperCase(rackingSystemSb.charAt(0)));

            return rackingSystemSb.toString();
        } else {
            return "";
        }
    }
}
