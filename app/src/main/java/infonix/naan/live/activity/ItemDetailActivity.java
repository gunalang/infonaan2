package infonix.naan.live.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import infonix.naan.live.MainApplication;
import infonix.naan.live.R;
import infonix.naan.live.adapter.DetailTabAdapter;
import infonix.naan.live.adapter.TaskViewpagerAdapter;
import infonix.naan.live.listener.ObservableObject;
import infonix.naan.live.model.CallStats.CallStatsModel;
import infonix.naan.live.model.menu.ArgData;
import infonix.naan.live.network.ApiService;
import infonix.naan.live.service.CallRecorderServiceAll;
import infonix.naan.live.utils.GlobalData;
import infonix.naan.live.utils.NetworkUtil;
import infonix.naan.live.view.fab.FloatingActionButton;
import infonix.naan.live.view.fab.FloatingActionMenu;
import infonix.naan.live.view.jcplayer.JcPlayerService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemDetailActivity extends AppCompatActivity {

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.frame)
    CoordinatorLayout frame;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.menu_fab)
    FloatingActionMenu menuFab;
    @BindView(R.id.fab_appointment)
    FloatingActionButton appointment;
    @BindView(R.id.fab_call)
    FloatingActionButton call;
    @BindView(R.id.fab_task)
    FloatingActionButton task;

    private ProgressDialog progressDialog;
    private DetailTabAdapter tabAdapter;
    private String headerText = "Timeline";
    private String layoutName;
    public static int colorCode = 0;
    private String url;
    private String method;
    private String params;
    private ArrayList<ArgData> argData = new ArrayList<>();
    private Gson gson;
    private HashMap<String, String> paramsList = new HashMap<>();
    private int screenHeight, screenWidth;

    private Calendar myCalendar;
    private SimpleDateFormat sdf;

    Call<CallStatsModel> apiCall;
    private ApiService restService;
    private CallStatsModel callListDataModel;

    private ArrayList<String> tabTitles;
    private ArrayList<View> views;

    private int PERMISSION_CODE = 23;
    private JcPlayerService jcPlayerService;
    boolean mBounded;
    private Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        restService = ((MainApplication) getApplication()).getClient();
        ButterKnife.bind(this);
        myCalendar = Calendar.getInstance();
        sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        Bundle bundle = getIntent().getExtras();
        gson = new Gson();
        if (bundle != null) {
            headerText = bundle.getString("title");
            layoutName = bundle.getString("layoutId");
            colorCode = bundle.getInt("color");
            url = bundle.getString("url");
            method = bundle.getString("method");
            params = bundle.getString("params");
            if (params != null)
                argData = gson.fromJson(params, new TypeToken<ArrayList<ArgData>>() {
                }.getType());
            if (argData != null && argData.size() > 0) {
                for (ArgData argData : argData) {
                    paramsList.put(argData.getFldName(), argData.getFldValue());
                }
            }
        }

        setToolbarItem(headerText);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;

        tabAdapter = new DetailTabAdapter(getSupportFragmentManager(), 3);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(tabAdapter);
        viewPager.setCurrentItem(1);

        menuFab.setClosedOnTouchOutside(true);
        if (colorCode != 0) {
            menuFab.setMenuButtonColorNormal(colorCode);
            appointment.setColorNormal(colorCode);
            call.setColorNormal(colorCode);
            task.setColorNormal(colorCode);
            menuFab.setMenuButtonColorPressed(colorCode);
            appointment.setColorPressed(colorCode);
            call.setColorPressed(colorCode);
            task.setColorPressed(colorCode);
        }else {
            menuFab.setMenuButtonColorNormal(R.color.colorPrimary);
            appointment.setColorNormal(R.color.colorPrimary);
            call.setColorNormal(R.color.colorPrimary);
            task.setColorNormal(R.color.colorPrimary);
            menuFab.setMenuButtonColorPressed(R.color.colorPrimary);
            appointment.setColorPressed(R.color.colorPrimary);
            call.setColorPressed(R.color.colorPrimary);
            task.setColorPressed(R.color.colorPrimary);
        }
        appointment.setOnClickListener(clickListener);
        call.setOnClickListener(clickListener);
        task.setOnClickListener(clickListener);
        menuFab.setOnMenuButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (menuFab.isOpened()) {
                    // Toast.makeText(ItemDetailActivity.this, menuRed.getMenuButtonLabelText(), Toast.LENGTH_SHORT).show();
                }

                menuFab.toggle(true);
            }
        });
        menuFab.setVisibility(View.GONE);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    fabVisible(false);
                    if (jcPlayerService != null)
                        jcPlayerService.destroy();
                } else if (tab.getPosition() == 1) {
                    fabVisible(true);
                    if (jcPlayerService != null)
                        jcPlayerService.destroy();
                } else if (tab.getPosition() == 2) {
                    fabVisible(false);
                    ObservableObject.getInstance().updateValue("refreshCallList");
                    if (jcPlayerService != null)
                        jcPlayerService.destroy();
                }
                hideKeyboard();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        if (colorCode != 0)
            tabLayout.setBackgroundColor(colorCode);
        else
            tabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        if (new NetworkUtil().isNetworkAvailable(getBaseContext())) {
            if (url != null && method != null) {
                getCallStatsTask();
            } else
                Toast.makeText(getApplicationContext(), "No url/method connection", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getBaseContext(), "No internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent mIntent = new Intent(this, JcPlayerService.class);
        bindService(mIntent, mConnection, BIND_AUTO_CREATE);
    }

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            //Service is disconnected
            mBounded = false;
            jcPlayerService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            //Service is connected
            mBounded = true;
            JcPlayerService.JcPlayerServiceBinder binder = (JcPlayerService.JcPlayerServiceBinder) service;
            jcPlayerService = binder.getService();
        }
    };

    public void getCallStatsTask() {
        progressDialog = new ProgressDialog(ItemDetailActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        try {

            if (method.equalsIgnoreCase("POST")) {
                apiCall = restService.postMthodCallStats(url, paramsList);
            } else {
                StringBuilder p = new StringBuilder();
                p.setLength(0);
                for (int i = 0; i < argData.size(); i++) {
                    if (!argData.get(i).getFldName().equalsIgnoreCase("id")) {
                        p.append("&").append(argData.get(i).getFldName()).append("=").append(argData.get(i).getFldValue());
                    } else {
                        p.append("&").append(argData.get(i).getFldName()).append("=").append(argData.get(i).getFldValue());
                        Toast.makeText(getApplicationContext(), argData.get(i).getFldValue(), Toast.LENGTH_SHORT).show();
                        //p.append("&").append(argData.get(i).getFldName()).append("=").append("3059");
                    }
                }
                String a = p.toString();
                apiCall = restService.getMethodCallStats(url + "?" + a);
            }
            apiCall.enqueue(new Callback<CallStatsModel>() {
                @Override
                public void onResponse(@NonNull Call<CallStatsModel> call, @NonNull Response<CallStatsModel> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    callListDataModel = response.body();
                    GlobalData.callStatsModel = callListDataModel;
                    if (callListDataModel != null) {
                        viewPager.removeAllViews();
                        viewPager.setAdapter(tabAdapter);
                        viewPager.setOffscreenPageLimit(3);
                        viewPager.setCurrentItem(1);
                        if (callListDataModel.getCompanyData() != null &&
                                callListDataModel.getCompanyData().getClientName() != null)
                            setToolbarItem(callListDataModel.getCompanyData().getClientName());
                        fabVisible(true);
                    } else {
                        menuFab.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "No Data Found", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<CallStatsModel> call, @NonNull Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (!t.getMessage().equalsIgnoreCase("Canceled"))
                        new android.support.v7.app.AlertDialog.Builder(ItemDetailActivity.this)
                                .setMessage("No Data Found")
                                .setNegativeButton("close", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbarItem(String title) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //   getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (colorCode != 0)
                window.setStatusBarColor(colorCode);
            else
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimary, null));
        }
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //toolbar color
            if (getSupportActionBar() != null) {
                if (colorCode != 0)
                    Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorCode));
                else
                    Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            }
            //toolbar text color
            SpannableString s = new SpannableString(firstUpperCase(title));
            s.setSpan(new ForegroundColorSpan(Color.WHITE), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if (getSupportActionBar() != null)
                Objects.requireNonNull(getSupportActionBar()).setTitle(s);
        }
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.fab_appointment:
                    ObservableObject.getInstance().updateValue("hide bottomsheet");
                    menuFab.close(true);
                    break;
                case R.id.fab_call:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (isReadAccountAllowed()) {
                            Intent opt = new Intent(ItemDetailActivity.this, CallRecorderServiceAll.class);
                            startService(opt);
                            callIntent();
                        } else {
                            requestPermission();
                        }
                    } else {
                        Intent opt = new Intent(ItemDetailActivity.this, CallRecorderServiceAll.class);
                        startService(opt);
                        callIntent();
                    }
                    menuFab.close(true);
                    break;
                case R.id.fab_task:
                    addNew("New Task");
                    menuFab.close(true);
                    break;
            }
        }
    };

    private boolean isReadAccountAllowed() {
        //Getting the permission status
        int result1 = ContextCompat.checkSelfPermission(ItemDetailActivity.this, Manifest.permission.CALL_PHONE);
        int result2 = ContextCompat.checkSelfPermission(ItemDetailActivity.this, Manifest.permission.RECORD_AUDIO);


        //If permission is granted returning true
        return result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED;

        //If permission is not granted returning false
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(ItemDetailActivity.this, Manifest.permission.RECORD_AUDIO)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        if (ActivityCompat.shouldShowRequestPermissionRationale(ItemDetailActivity.this, Manifest.permission.CALL_PHONE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(ItemDetailActivity.this, new String[]{
                        Manifest.permission.RECORD_AUDIO, Manifest.permission.CALL_PHONE},
                PERMISSION_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callIntent();
            } else {
                requestPermission();
            }
        }
    }

    private void fabVisible(boolean v) {
        if (v) {
            ViewCompat.animate(menuFab).scaleX(1.0F).scaleY(1.0F).alpha(1.0F)
                    .setInterpolator(INTERPOLATOR).withLayer().setListener(null)
                    .start();
            menuFab.setVisibility(View.VISIBLE);
        } else {
            ViewCompat.animate(menuFab).scaleX(1.0F).scaleY(1.0F).alpha(1.0F)
                    .setInterpolator(INTERPOLATOR).withLayer().setListener(null)
                    .start();
            menuFab.setVisibility(View.GONE);
        }
    }

    private void callIntent() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode("121")));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void addNew(String validation) {
       /* final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                ItemDetailActivity.this, R.style.color_dialog);*/
        // final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(ItemDetailActivity.this);
        final Dialog bottomSheetDialog = new Dialog(ItemDetailActivity.this, R.style.BottomDialogs);
        View view1 = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.new_task_layout, null);

        LinearLayout linearLayout = view1.findViewById(R.id.ll_childview);
        LinearLayout frame = view1.findViewById(R.id.ll_frame);
        ScrollView scrollView = view1.findViewById(R.id.scrollView);
        CoordinatorLayout coordinatorLayout = view1.findViewById(R.id.ll_new);

        ImageView close = view1.findViewById(R.id.iv_close);
        TextView title = view1.findViewById(R.id.tv_title);
        title.setText(validation);
        if (validation.equalsIgnoreCase("New Contact")) {
            for (int i = 0; i < 5; i++) {
                View spinnerView = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.spinner, linearLayout, false);
                Spinner spinner = spinnerView.findViewById(R.id.spinner);
                View textViewView = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.textview, linearLayout, false);
                TextView textView = textViewView.findViewById(R.id.tv_text);
                textView.setTextColor(getResources().getColor(R.color.editboxheading));
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_small));
                View editView = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.edittext, linearLayout, false);
                EditText editText = editView.findViewById(R.id.edittext);
                editText.setFocusable(true);
                if (i == 4) {
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
                    lp.setMargins(6, 0, 6, 0);
                    editText.setLayoutParams(lp);
                    editText.setGravity(Gravity.START);
                    editText.setSingleLine(false);

                    editText.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View v, MotionEvent event) {
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                                case MotionEvent.ACTION_UP:
                                    v.getParent().requestDisallowInterceptTouchEvent(false);
                                    break;
                            }
                            return false;
                        }
                    });
                }
                if (i == 0)
                    textView.setText("Name");
                else if (i == 1)
                    textView.setText("Designation");
                else if (i == 2)
                    textView.setText("Phone");
                else if (i == 3)
                    textView.setText("Email");
                else
                    textView.setText("Comments");

                linearLayout.addView(textViewView);
                linearLayout.addView(editText);
            }
        } else if (validation.equalsIgnoreCase("new Task")) {
            for (int i = 0; i < 5; i++) {
                View spinnerView = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.spinner, linearLayout, false);
                Spinner spinner = spinnerView.findViewById(R.id.spinner);
                View textViewView = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.textview, linearLayout, false);
                TextView textView = textViewView.findViewById(R.id.tv_text);
                textView.setTextColor(getResources().getColor(R.color.editboxheading));
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_small));
                View editView = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.edittext, linearLayout, false);
                EditText editText = editView.findViewById(R.id.edittext);
                editText.setFocusable(true);
                View dateView = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.datepicker, linearLayout, false);
                final TextView datePicker = dateView.findViewById(R.id.tv_date);
                datePicker.setText(sdf.format(myCalendar.getTime()));
                datePicker.setGravity(Gravity.START);
                datePicker.setPadding(10, 0, 0, 0);

                datePicker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ItemDetailActivity.this != null) {
                            DatePickerDialog datePickerDialogFrom = new DatePickerDialog(ItemDetailActivity.this,
                                    new DatePickerDialog.OnDateSetListener() {
                                        @Override
                                        public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                              int dayOfMonth) {
                                            myCalendar.set(Calendar.YEAR, year);
                                            myCalendar.set(Calendar.MONTH, monthOfYear);
                                            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                            datePicker.setText(sdf.format(myCalendar.getTime()));
                                        }
                                    }, myCalendar
                                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                    myCalendar.get(Calendar.DAY_OF_MONTH));
                            //datePickerDialogFrom.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                            datePickerDialogFrom.show();
                        }
                    }
                });
                if (i == 0)
                    textView.setText("Assigned By");
                else if (i == 1)
                    textView.setText("Assigned To");
                else if (i == 2)
                    textView.setText("Action");
                else if (i == 3)
                    textView.setText("Complete By");
                else
                    textView.setText("Comments");

                if (i == 4) {
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
                    lp.setMargins(6, 0, 6, 0);
                    editText.setLayoutParams(lp);
                    editText.setGravity(Gravity.START);
                    editText.setSingleLine(false);

                    editText.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View v, MotionEvent event) {
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                                case MotionEvent.ACTION_UP:
                                    v.getParent().requestDisallowInterceptTouchEvent(false);
                                    break;
                            }
                            return false;
                        }
                    });
                }

                ArrayList<String> attendbyItems = new ArrayList<>();
                attendbyItems.add("Ignatious");
                attendbyItems.add("Abhi");
                attendbyItems.add("Anu");
                ArrayList<String> actionItems = new ArrayList<>();
                actionItems.add("Send Proposal");
                actionItems.add("Demo");
                actionItems.add("Direct Visit");
                if (ItemDetailActivity.this != null) {
                    ArrayAdapter<String> attendbyAdapter = new ArrayAdapter<String>(ItemDetailActivity.this,
                            android.R.layout.simple_dropdown_item_1line, attendbyItems);
                    ArrayAdapter<String> actionAdapter = new ArrayAdapter<String>(ItemDetailActivity.this,
                            android.R.layout.simple_dropdown_item_1line, actionItems);
                    if (i <= 1)
                        spinner.setAdapter(attendbyAdapter);
                    else
                        spinner.setAdapter(actionAdapter);
                }
                linearLayout.addView(textViewView);
                if (i <= 2)
                    linearLayout.addView(spinnerView);
                else if (i == 3)
                    linearLayout.addView(dateView);
                else
                    linearLayout.addView(editView);
            }
        } else {
            for (int i = 0; i < 3; i++) {
                View spinnerView = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.spinner, linearLayout, false);
                Spinner spinner = spinnerView.findViewById(R.id.spinner);
                View textViewView = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.textview, linearLayout, false);
                TextView textView = textViewView.findViewById(R.id.tv_text);
                textView.setTextColor(getResources().getColor(R.color.editboxheading));
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_small));
                View editView = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.edittext, linearLayout, false);
                EditText editText = editView.findViewById(R.id.edittext);
                editText.setFocusable(true);
                if (i == 0)
                    textView.setText("Notes By");
                else if (i == 1)
                    textView.setText("Flag");
                else
                    textView.setText("Details");

                ArrayList<String> attendbyItems = new ArrayList<>();
                attendbyItems.add("Ignatious");
                attendbyItems.add("Abhi");
                attendbyItems.add("Anu");
                ArrayList<String> flagItems = new ArrayList<>();
                flagItems.add("White");
                flagItems.add("Yellow");
                flagItems.add("Red");
                if (ItemDetailActivity.this != null) {
                    ArrayAdapter<String> attendbyAdapter = new ArrayAdapter<String>(ItemDetailActivity.this,
                            android.R.layout.simple_dropdown_item_1line, attendbyItems);
                    ArrayAdapter<String> flagAdapter = new ArrayAdapter<String>(ItemDetailActivity.this,
                            android.R.layout.simple_dropdown_item_1line, flagItems);
                    if (i == 0)
                        spinner.setAdapter(attendbyAdapter);
                    else spinner.setAdapter(flagAdapter);
                }
                if (i == 2) {
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
                    lp.setMargins(6, 0, 6, 0);
                    editText.setLayoutParams(lp);
                    editText.setGravity(Gravity.START);
                    editText.setSingleLine(false);
                }

                linearLayout.addView(textViewView);
                if (i <= 1)
                    linearLayout.addView(spinnerView);
                else
                    linearLayout.addView(editText);
            }
        }
        View buttonView = LayoutInflater.from(ItemDetailActivity.this).inflate(R.layout.button, linearLayout, false);
        Button submit = buttonView.findViewById(R.id.button1);
        submit.setText("Submit");
        submit.setTextColor(Color.BLACK);
        submit.setBackgroundColor(Color.TRANSPARENT);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                hideKeyboard();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                hideKeyboard();
            }
        });
        linearLayout.addView(buttonView);
      /*  alertDialogBuilder
                .setView(view1)
                .setCancelable(true);
        alertDialog = alertDialogBuilder
                .create();
        alertDialog.show();*/
        bottomSheetDialog.setContentView(view1);

        bottomSheetDialog.setCancelable(true);
        if (bottomSheetDialog.getWindow() != null) {
            bottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            bottomSheetDialog.getWindow().setGravity(Gravity.TOP);
        }
        bottomSheetDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        bottomSheetDialog.show();
    }

    private String firstUpperCase(String value) {
        if (value != null && value.length() > 0) {
            StringBuilder rackingSystemSb = new StringBuilder(value);
            rackingSystemSb.setCharAt(0, Character.toUpperCase(rackingSystemSb.charAt(0)));

            return rackingSystemSb.toString();
        } else {
            return "";
        }
    }

    @Override
    protected void onPause() {
        if (jcPlayerService != null)
            jcPlayerService.destroy();
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (mBounded) {
            unbindService(mConnection);
            mBounded = false;
        }
        GlobalData.callStatsModel = null;
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (imm != null && getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void showKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(view, 0);
        }
    }
}
