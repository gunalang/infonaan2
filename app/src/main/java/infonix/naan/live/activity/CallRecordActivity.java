package infonix.naan.live.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telecom.Call;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import infonix.naan.live.MainApplication;
import infonix.naan.live.R;
import infonix.naan.live.adapter.CallRecordAdapter;
import infonix.naan.live.dbHelper.HelperCallRecordings;
import infonix.naan.live.model.CallRecordListModel;
import infonix.naan.live.network.ApiService;
import infonix.naan.live.service.CallRecorderServiceAll;
import infonix.naan.live.service.CallRecorderServiceOptional;
import infonix.naan.live.utils.DateTimeUtils;
import infonix.naan.live.view.SpacesItemDecoration;

public class CallRecordActivity extends AppCompatActivity implements CallRecordAdapter.AdapterInterface {

    @BindView(R.id.frame)
    CoordinatorLayout frame;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_empty)
    TextView empty;
    @BindView(R.id.design_bottom_sheet)
    View bottomSheet;
    BottomSheetBehavior behavior;

    private Cursor c;
    HelperCallRecordings hcr;
    SQLiteDatabase sdb;
    LayoutInflater layoutInflater;
    CallRecordAdapter mAdapter;
    private ArrayList<CallRecordListModel> callListModelArrayList;
    private Uri filePath;
    private Animation removeAnimation;

    private int PERMISSION_CODE = 23;
    private ApiService restService;
    private String headerText = "Timeline";
    private int colorCode = 0;
    Handler handler = new Handler();

    private String[] options = new String[]{"dont ask, record all", "ask every time"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_record);

        ButterKnife.bind(this);

        restService = ((MainApplication) getApplication()).getClient();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            headerText = bundle.getString("title");
            colorCode = bundle.getInt("color");
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (colorCode != 0)
                window.setStatusBarColor(colorCode);
        }
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //toolbar color
            if (getSupportActionBar() != null && colorCode != 0)
                Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorCode));
            //toolbar text color
            SpannableString s = new SpannableString(headerText);
            s.setSpan(new ForegroundColorSpan(Color.WHITE), 0, headerText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if (getSupportActionBar() != null)
                Objects.requireNonNull(getSupportActionBar()).setTitle(s);
        }
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recyclerview_space_medium);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        display();
    }

    private void display() {
        hcr = new HelperCallRecordings(this);
        c = hcr.display();

        try {
            sdb = hcr.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Cursor cursor = sdb.rawQuery("select * from CallRecords", null);

        if (cursor.getCount() == 0) {
            Log.e("log", "err");
            empty.setVisibility(View.VISIBLE);
        } else {
            cursor.moveToFirst();
            callListModelArrayList = new ArrayList<CallRecordListModel>();
            do {
                CallRecordListModel note = new CallRecordListModel();
                note.setTime(cursor.getString(cursor.getColumnIndex("Time")));
                note.setNumber(cursor.getString(cursor.getColumnIndex("Number")));
                note.setCallType(cursor.getString(cursor.getColumnIndex("CallType")));
                note.setFilePath(cursor.getString(cursor.getColumnIndex("FilePath")));
                note.setStatus(cursor.getString(cursor.getColumnIndex("status")));

                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
                String stime = sdf.format(new Date(note.getTime()));

                sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                String sdate = sdf.format(new Date(note.getTime()));

                note.setTime(stime + " " + sdate);
                Uri uri = Uri.parse(note.getFilePath());
                int millSecond = 0;
                try {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(this, uri);
                    String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                    millSecond = Integer.parseInt(durationStr);
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
                note.setDuration(millSecond / (60 * 1000) + " min " + (millSecond / 1000) % (60) + " sec");
                callListModelArrayList.add(note);
            } while (cursor.moveToNext());
        }
        if (callListModelArrayList != null && callListModelArrayList.size() > 0) {
            recyclerView.setClipToPadding(false);
            recyclerView.setPadding(0, 0, 0, (int) getResources().getDimension(R.dimen.bottom_sheet_hight));
            Collections.reverse(callListModelArrayList);
            mAdapter = new CallRecordAdapter(layoutInflater, callListModelArrayList, this, this,
                    recyclerView);
            recyclerView.setAdapter(mAdapter);
            bottomSheet.setVisibility(View.GONE);
            empty.setVisibility(View.GONE);
        } else {
            bottomSheet.setVisibility(View.GONE);
            empty.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
       /* if (id == R.id.action_option) {
            callRecordDialog();
        } else*/
        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.action_call) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isReadAccountAllowed()) {
                    Intent opt = new Intent(this, CallRecorderServiceAll.class);
                    startService(opt);
                    callIntent();
                } else {
                    requestPermission();
                }
            } else {
                Intent opt = new Intent(this, CallRecorderServiceAll.class);
                startService(opt);
                callIntent();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void callIntent() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode("121")));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    private boolean isReadAccountAllowed() {
        //Getting the permission status
        int result1 = ContextCompat.checkSelfPermission(CallRecordActivity.this, Manifest.permission.CALL_PHONE);
        int result2 = ContextCompat.checkSelfPermission(CallRecordActivity.this, Manifest.permission.RECORD_AUDIO);


        //If permission is granted returning true
        return result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED;

        //If permission is not granted returning false
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(CallRecordActivity.this, Manifest.permission.RECORD_AUDIO)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        if (ActivityCompat.shouldShowRequestPermissionRationale(CallRecordActivity.this, Manifest.permission.CALL_PHONE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(CallRecordActivity.this, new String[]{
                        Manifest.permission.RECORD_AUDIO, Manifest.permission.CALL_PHONE},
                PERMISSION_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callIntent();
            } else {
                requestPermission();
            }
        }
    }

    @Override
    public void deleteandset(final int position) {
        c.moveToPosition(position);
        File file = new File(c.getString(2));

        HelperCallRecordings hcr = new HelperCallRecordings(MainApplication.getInstance());

        hcr.clearData(c.getString(c.getColumnIndex("Time")));

        c = hcr.display();

        if (c.getCount() == 0) {
            Toast.makeText(getApplicationContext(), "No data found", Toast.LENGTH_SHORT).show();
        }
        callListModelArrayList.remove(position);
        mAdapter.notifyItemRemoved(position);
        if (callListModelArrayList.size() > 0)
            bottomSheet.setVisibility(View.GONE);
        hcr.closeDatabase();

        if (file.exists()) {
            file.delete();
            Toast.makeText(MainApplication.getInstance(), "File deleted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainApplication.getInstance(), "File does not exist", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        if (handler != null)
            handler.removeCallbacksAndMessages(null);

        super.onPause();
    }

}
