package infonix.naan.live.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.wifi.hotspot2.pps.HomeSp;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import infonix.naan.live.MainApplication;
import infonix.naan.live.R;
import infonix.naan.live.adapter.ExpandLayoutAdapter;
import infonix.naan.live.adapter.HomeDetailAdapter;
import infonix.naan.live.adapter.TaskViewpagerAdapter;
import infonix.naan.live.fragment.SecondStageHomeFragment;
import infonix.naan.live.model.CallListItemModel;
import infonix.naan.live.model.CallListModel;
import infonix.naan.live.model.menu.ArgData;
import infonix.naan.live.model.menu.BlockContent;
import infonix.naan.live.model.menu.BlockContentFilters;
import infonix.naan.live.model.menu.ChildBlockData;
import infonix.naan.live.model.menu.MenuDataModel;
import infonix.naan.live.network.ApiService;
import infonix.naan.live.utils.GlobalData;
import infonix.naan.live.utils.NetworkUtil;
import infonix.naan.live.view.TouchDetectableScrollView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeDetailActivity extends AppCompatActivity implements HomeDetailAdapter.GroupitemClickListener {

    @BindView(R.id.frame)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.ll_parentview)
    LinearLayout parentLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;
    @BindView(R.id.loadmore_progressbar)
    ProgressBar loadmoreProgressBar;
    @BindView(R.id.nestedscrollview)
    TouchDetectableScrollView nestedScrollView;

    private Menu menu;
    private MenuItem groupMenuIcon, filterMenuIcon;
    private int screenHeight, screenWidth;


    private String headerText = "Details";
    private int colorCode = 0;
    long delayTime = 1000;
    Call<MenuDataModel> apiCall;
    private String layoutId;
    private String title, subTitle;
    private String url;
    private String method;
    private String params;
    private ArrayList<ArgData> argData = new ArrayList<>();
    private Gson gson;
    private HashMap<String, String> paramsList = new HashMap<>();

    private Random random = new Random(System.currentTimeMillis());
    Handler handler = new Handler();

    private WeakReference<HomeDetailActivity> homeActivityWeakReference;

    private ApiService restService;
    private MenuDataModel menuDataModel;
    private ArrayList<String> sortingTitleList = new ArrayList<>();
    private ArrayList<BlockContent> callListItemModelArrayList = new ArrayList<>();
    private ArrayList<BlockContent> allCallListList = new ArrayList<>();
    private ArrayList<BlockContentFilters> callFiltersArrayList = new ArrayList<>();
    private HomeDetailAdapter homeDetailAdapter;
    private String datePattern = "([0-9]{2}) ([a-zA-Z]{3}) ([0-9]{4})";
    Typeface face;
    private boolean isGrouping;
    private int itemCount = 20;
    private boolean showEndMessage;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_detail);

        ButterKnife.bind(this);

        restService = ((MainApplication) getApplication()).getClient();
        //face = Typeface.createFromAsset(getAssets(), "montserrat-regular.ttf");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;

        homeActivityWeakReference = new WeakReference<>(this);
        gson = new Gson();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            headerText = bundle.getString("title");
            subTitle = bundle.getString("sub_title");
            layoutId = bundle.getString("layoutId");
            colorCode = bundle.getInt("color");
            url = bundle.getString("url");
            method = bundle.getString("method");
            params = bundle.getString("params");
            if (params != null)
                argData = gson.fromJson(params, new TypeToken<ArrayList<ArgData>>() {
                }.getType());
            if (argData != null && argData.size() > 0) {
                for (ArgData argData : argData) {
                    paramsList.put(argData.getFldName(), argData.getFldValue());
                }
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (colorCode != 0)
                window.setStatusBarColor(colorCode);
            else
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimary, null));
        }
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //toolbar color
            if (getSupportActionBar() != null) {
                if (colorCode != 0)
                    Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorCode));
                else
                    Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
                //toolbar text color
                if (headerText != null) {
                    SpannableString s = new SpannableString(headerText);
                    s.setSpan(new ForegroundColorSpan(Color.WHITE), 0, headerText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    if (getSupportActionBar() != null)
                        Objects.requireNonNull(getSupportActionBar()).setTitle(s);
                }
                if (subTitle != null)
                    getSupportActionBar().setSubtitle(subTitle);
            }
        }
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (new NetworkUtil().isNetworkAvailable(getBaseContext())) {
                    new android.support.v7.app.AlertDialog.Builder(HomeDetailActivity.this)
                            .setMessage("Do you want refresh/reset?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (url != null && method != null) {
                                        getCallListTask();
                                    } else
                                        Toast.makeText(getApplicationContext(), "No url/method connection", Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    swipeRefreshLayout.setRefreshing(false);
                                }
                            })
                            .show();
                } else {
                    Toast.makeText(getBaseContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        if (new NetworkUtil().isNetworkAvailable(getBaseContext())) {
            if (url != null && method != null) {
                getCallListTask();
            } else
                Toast.makeText(getApplicationContext(), "No url/method connection", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getBaseContext(), "No internet connection", Toast.LENGTH_SHORT).show();
        }

        nestedScrollView.setMyScrollChangeListener(new TouchDetectableScrollView.OnMyScrollChangeListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onBottomReached() {
                if (allCallListList.size() > 0 && isGrouping) {
                    int addeditemCount = callListItemModelArrayList.size();//10
                    int totalCount = allCallListList.size();//25
                    int pendingItemCount = totalCount - addeditemCount;//15
                    if (pendingItemCount > 0) {
                        loadmoreProgressBar.setVisibility(View.VISIBLE);
                        if (pendingItemCount > itemCount) {
                            for (int i = addeditemCount; i < addeditemCount + itemCount; i++) {
                                callListItemModelArrayList.add(allCallListList.get(i));
                            }
                            handler.postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    homeDetailAdapter.notifyDataSetChanged();
                                    loadmoreProgressBar.setVisibility(View.GONE);
                                    nestedScrollView.smoothScrollTo(0, 50);
                                }
                            }, delayTime);
                        } else {
                            for (int i = addeditemCount; i < allCallListList.size(); i++) {
                                callListItemModelArrayList.add(allCallListList.get(i));
                            }
                            handler.postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    homeDetailAdapter.notifyDataSetChanged();
                                    loadmoreProgressBar.setVisibility(View.GONE);
                                    nestedScrollView.smoothScrollTo(0, 50);
                                }
                            }, delayTime);
                            showEndMessage = true;
                        }
                    } else {
                        if (showEndMessage) {
                            Toast.makeText(getBaseContext(), "No more Data", Toast.LENGTH_SHORT).show();
                            showEndMessage = false;
                        }
                        // api call for pagination
                    }
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
        if (apiCall != null)
            apiCall.cancel();
        super.onPause();
    }

    @Override
    public void groupitemClickEvent(int position, String targetLocationId, String linkName, String linkId, String layout,
                                    String url, String method, ArrayList<ArgData> argDataArrayList) {
        if (menuDataModel != null && menuDataModel.getData() != null) {
            if (targetLocationId != null && targetLocationId.equalsIgnoreCase("0")) {
                if (layout != null && layout.equalsIgnoreCase("gridView1")) {
                    Intent bundle = new Intent(getBaseContext(), ItemDetailActivity.class);
                    bundle.putExtra("title", linkName);
                    bundle.putExtra("color", colorCode);
                    bundle.putExtra("url", url);
                    bundle.putExtra("layout", layout);
                    bundle.putExtra("method", method);
                    Gson gson = new Gson();
                    JsonElement jsonElement = gson.toJsonTree(argDataArrayList,
                            new TypeToken<ArrayList<ArgData>>() {
                            }.getType());
                    bundle.putExtra("params", jsonElement.toString());
                    startActivity(bundle);
                }
            } else {
                Intent bundle = new Intent(getBaseContext(), HomeDetailActivity.class);
                bundle.putExtra("title", linkName);
                bundle.putExtra("color", colorCode);
                bundle.putExtra("url", url);
                bundle.putExtra("layout", layout);
                bundle.putExtra("method", method);
                Gson gson = new Gson();
                JsonElement jsonElement = gson.toJsonTree(argDataArrayList,
                        new TypeToken<ArrayList<ArgData>>() {
                        }.getType());
                bundle.putExtra("params", jsonElement.toString());
                startActivity(bundle);
            }
        } else {
            Toast.makeText(getApplicationContext(), "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void callOptionClickEvent(String linkName) {
        Intent intent = new Intent(getBaseContext(), CallRecordActivity.class);
        intent.putExtra("title", linkName);
        intent.putExtra("color", colorCode);
        startActivity(intent);
    }

    public void getCallListTask() {
        progressBar.setVisibility(View.VISIBLE);
        try {

            if (method.equalsIgnoreCase("POST")) {
                apiCall = restService.postMethodCallList(url, paramsList);
            } else {
                StringBuilder p = new StringBuilder();
                p.setLength(0);
                for (int i = 0; i < argData.size(); i++) {
                    p.append("&").append(argData.get(i).getFldName()).append("=").append(argData.get(i).getFldValue());
                }
                apiCall = restService.getMethodCallList(url + "?" + p.toString());
            }
            apiCall.enqueue(new Callback<MenuDataModel>() {
                @Override
                public void onResponse(@NonNull Call<MenuDataModel> call, @NonNull Response<MenuDataModel> response) {
                    progressBar.setVisibility(View.GONE);

                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                        swipeRefreshLayout.setRefreshing(false);
                    groupMenuIcon.setVisible(false);
                    filterMenuIcon.setVisible(false);
                    menuDataModel = response.body();
                    if (menuDataModel != null && menuDataModel.getData() != null) {
                        parentLayout.removeAllViews();
                        if (menuDataModel.getData() != null && menuDataModel.getData().size() > 0) {
                            for (int i = 0; i < menuDataModel.getData().size(); i++) {
                                if (menuDataModel.getData().get(i).getNaanMasterPage()
                                        .getChildBlockData() != null && menuDataModel.getData().get(i).getNaanMasterPage()
                                        .getChildBlockData().size() > 0) {
                                    for (int j = 0; j < menuDataModel.getData().get(i).getNaanMasterPage()
                                            .getChildBlockData().size(); j++) {
                                        if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                .get(j).getBlockTemplateName().equalsIgnoreCase("transRecyclerView")) {

                                        } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                .get(j).getBlockTemplateName().equalsIgnoreCase("horizRecyclerView")) {

                                        } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                .get(j).getBlockTemplateName().equalsIgnoreCase("gridRecyclerView")) {
                                            //gridview

                                        } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                .get(j).getBlockTemplateName().equalsIgnoreCase("listRecyclerView")) {
                                            //recyclerview
                                            if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockName().equalsIgnoreCase("callListBlock")) {
                                                sortingTitleList.clear();
                                                callFiltersArrayList = menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData().get(j).getBlockContentFilters();
                                                if (callFiltersArrayList != null && callFiltersArrayList.size() > 0) {
                                                    for (int k = 0; k < callFiltersArrayList.size(); k++) {
                                                        sortingTitleList.add(callFiltersArrayList.get(k).getTitleText());
                                                    }
                                                }
                                                allCallListList = menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData().get(j).getBlockContent();
                                                if (allCallListList != null && allCallListList.size() > 0) {
                                                    Collections.sort(allCallListList, new Comparator<BlockContent>() {
                                                        DateFormat f = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);

                                                        @Override
                                                        public int compare(BlockContent lhs, BlockContent rhs) {

                                                            if (lhs.getLinkExtraContent().getNextCallDate().trim().matches(datePattern) &&
                                                                    rhs.getLinkExtraContent().getNextCallDate().trim().matches(datePattern)) {
                                                                try {
                                                                    return f.parse(lhs.getLinkExtraContent().getNextCallDate()).compareTo(f.parse(rhs.getLinkExtraContent().getNextCallDate()));
                                                                } catch (ParseException e) {
                                                                    throw new IllegalArgumentException(e);
                                                                }
                                                            } else {
                                                                return lhs.getLinkExtraContent().getNextCallDate().compareTo(rhs.getLinkExtraContent().getNextCallDate());
                                                            }
                                                        }
                                                    });
                                                    if (parentLayout.getChildCount() > 0)
                                                        parentLayout.removeViewAt(0);
                                                    callListItemModelArrayList.clear();
                                                    Collections.reverse(allCallListList);
                                                    if (allCallListList.size() > itemCount) {
                                                        for (int l = 0; l < itemCount; l++) {
                                                            callListItemModelArrayList.add(allCallListList.get(l));
                                                        }
                                                    } else {
                                                        callListItemModelArrayList.addAll(allCallListList);
                                                    }
                                                    isGrouping = true;
                                                    verticalRecyclerView(callListItemModelArrayList, 0);
                                                    groupMenuIcon.setVisible(true);
                                                    filterMenuIcon.setVisible(true);
                                                } else {
                                                    groupMenuIcon.setVisible(false);
                                                    filterMenuIcon.setVisible(false);
                                                    Toast.makeText(getApplicationContext(), "No data found", Toast.LENGTH_SHORT).show();
                                                }
                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("timelineRecyclerView")) {
                                                //timeline

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("timelineRecyclerView")) {
                                                //timeline

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("tabView")) {
                                                //tab view

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("singleClmnTextBox")) {
                                                //single column textbox

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("doubleClmnTextBox")) {
                                                //double column textbox

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("fabButton")) {
                                                //fab buttons

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("audioPlayBox")) {
                                                //audiobox

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("audioList")) {
                                                //audio list

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("expandableList")) {
                                                //expandable list

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("singleClmnList")) {
                                                //single column list

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("doubleClmnList")) {
                                                //double column list

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("buttonList")) {
                                                //button list

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("chatView")) {
                                                //chat view

                                            } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                                    .get(j).getBlockTemplateName().equalsIgnoreCase("bottomSheet")) {
                                                //call bottom sheet
                                                bottomDialog();
                                            }
                                        }
                                    }
                                } else {
                                    noDataFound();
                                }
                            }
                        } else {
                            noDataFound();
                        }
                    } else {
                        noDataFound();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<MenuDataModel> call, @NonNull Throwable t) {
                    progressBar.setVisibility(View.GONE);

                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                        swipeRefreshLayout.setRefreshing(false);
                    if (!t.getMessage().equalsIgnoreCase("Canceled"))
                        new android.support.v7.app.AlertDialog.Builder(HomeDetailActivity.this)
                                .setMessage("Data parsing Failed")
                                .setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (url != null && method != null) {
                                            getCallListTask();
                                        } else
                                            Toast.makeText(getApplicationContext(), "No url/method connection", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                    }
                                })
                                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                }
            });

        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.sort, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        groupMenuIcon = menu.findItem(R.id.action_group);
        filterMenuIcon = menu.findItem(R.id.action_sort);

//        locationUpdateMenu.setVisible(isMyServiceRunning(StartLocationService.class));
        return true;
    }

    private void verticalRecyclerView(ArrayList<BlockContent> blockContentArrayList, int pos) {
        progressBar.setVisibility(View.VISIBLE);
        View view = LayoutInflater.from(HomeDetailActivity.this).inflate(R.layout.recyclerview, null);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeDetailActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setLayoutManager(new LinearLayoutManager(HomeDetailActivity.this));
        homeDetailAdapter = new HomeDetailAdapter(homeActivityWeakReference, blockContentArrayList,
                HomeDetailActivity.this, pos, getApplicationContext());
        recyclerView.setAdapter(homeDetailAdapter);
        parentLayout.addView(view, 0, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        progressBar.setVisibility(View.GONE);
        recyclerView.smoothScrollToPosition(0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
           /* if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);*/
            finish();
        } else if (id == R.id.action_sort) {
            if (sortingTitleList != null && sortingTitleList.size() > 0) {
                sortDialog();
            } else {
                Toast.makeText(getApplicationContext(), "No sorting data", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.action_group) {
            if (sortingTitleList != null && sortingTitleList.size() > 0) {
                groupDialog();
            } else {
                Toast.makeText(getApplicationContext(), "No group data", Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void groupDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeDetailActivity.this);
        // builder.setTitle("select");
        final String[] list = sortingTitleList.toArray(new String[sortingTitleList.size()]);
        builder.setTitle("Group by");
        builder.setItems(list, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                isGrouping = true;
                if (item == 0) {
                    Collections.sort(callListItemModelArrayList, new Comparator<BlockContent>() {
                        DateFormat f = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);

                        @Override
                        public int compare(BlockContent lhs, BlockContent rhs) {

                            if (lhs.getLinkExtraContent().getNextCallDate().trim().matches(datePattern) &&
                                    rhs.getLinkExtraContent().getNextCallDate().trim().matches(datePattern)) {
                                try {
                                    return f.parse(lhs.getLinkExtraContent().getNextCallDate()).compareTo(f.parse(rhs.getLinkExtraContent().getNextCallDate()));
                                } catch (ParseException e) {
                                    throw new IllegalArgumentException(e);
                                }
                            } else {
                                return lhs.getLinkExtraContent().getNextCallDate().compareTo(rhs.getLinkExtraContent().getNextCallDate());
                            }
                        }
                    });
                    if (callListItemModelArrayList.size() > 0) {
                        if (parentLayout.getChildCount() > 0)
                            parentLayout.removeViewAt(0);
                        Collections.reverse(callListItemModelArrayList);
                        verticalRecyclerView(callListItemModelArrayList, 0);
                    } else {
                        Toast.makeText(getApplicationContext(), "selected data not found", Toast.LENGTH_SHORT).show();
                    }
                } else if (item == 1) {
                    Collections.sort(callListItemModelArrayList, new Comparator<BlockContent>() {
                        @Override
                        public int compare(BlockContent lhs, BlockContent rhs) {
                            return (lhs.getLinkExtraContent().getCallPosition()).compareTo(rhs.getLinkExtraContent().getCallPosition());
                        }
                    });
                    if (callListItemModelArrayList.size() > 0) {
                        if (parentLayout.getChildCount() > 0)
                            parentLayout.removeViewAt(0);
                        verticalRecyclerView(callListItemModelArrayList, 1);
                    } else {
                        Toast.makeText(getApplicationContext(), "selected data not found", Toast.LENGTH_SHORT).show();
                    }
                } else if (item == 2) {
                    Collections.sort(callListItemModelArrayList, new Comparator<BlockContent>() {
                        @Override
                        public int compare(BlockContent lhs, BlockContent rhs) {
                            return (lhs.getLinkExtraContent().getClientType()).compareTo(rhs.getLinkExtraContent().getClientType());
                        }
                    });
                    if (callListItemModelArrayList.size() > 0) {
                        if (parentLayout.getChildCount() > 0)
                            parentLayout.removeViewAt(0);
                        verticalRecyclerView(callListItemModelArrayList, 2);
                    } else {
                        Toast.makeText(getApplicationContext(), "selected data not found", Toast.LENGTH_SHORT).show();
                    }
                }
                dialog.dismiss();


            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void sortDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeDetailActivity.this);
        // builder.setTitle("select");
        final String[] list = sortingTitleList.toArray(new String[sortingTitleList.size()]);
        builder.setTitle("Filter by");
        builder.setItems(list, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (callFiltersArrayList != null && callFiltersArrayList.size() > 0) {
                    sortOptionDialog(item, callFiltersArrayList.get(item).getTitleText());
                } else {
                    Toast.makeText(getApplicationContext(), "No options", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();


            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void sortOptionDialog(final int pos, String t) {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeDetailActivity.this);
        final String[] list = callFiltersArrayList.get(pos).getTitleData().toArray(
                new String[callFiltersArrayList.get(pos).getTitleData().size()]);
        builder.setTitle(t);
        builder.setItems(list, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                ArrayList<BlockContent> callArrayList = new ArrayList<>();
                for (int i = 0; i < allCallListList.size(); i++) {
                    if (pos == 0) {
                        if (allCallListList.get(i).getLinkExtraContent().getNextCallDate().equalsIgnoreCase(
                                callFiltersArrayList.get(pos).getTitleData().get(item)))
                            callArrayList.add(allCallListList.get(i));
                    } else if (pos == 1) {
                        if (allCallListList.get(i).getLinkExtraContent().getCallPosition().equalsIgnoreCase(
                                callFiltersArrayList.get(pos).getTitleData().get(item)))
                            callArrayList.add(allCallListList.get(i));
                    } else if (pos == 2) {
                        if (allCallListList.get(i).getLinkExtraContent().getClientType().equalsIgnoreCase(
                                callFiltersArrayList.get(pos).getTitleData().get(item)))
                            callArrayList.add(allCallListList.get(i));
                    }
                }
                if (callArrayList.size() > 0) {
                    if (parentLayout.getChildCount() > 0)
                        parentLayout.removeViewAt(0);
                    Collections.reverse(callArrayList);
                    isGrouping = false;
                    verticalRecyclerView(callArrayList, 0);
                } else {
                    Toast.makeText(getApplicationContext(), "selected data not found", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }

    private String firstUpperCase(String value) {
        if (value != null && value.length() > 0) {
            StringBuilder rackingSystemSb = new StringBuilder(value);
            rackingSystemSb.setCharAt(0, Character.toUpperCase(rackingSystemSb.charAt(0)));

            return rackingSystemSb.toString();
        } else {
            return "";
        }
    }

    private void bottomDialog() {
        final Dialog bottomSheetDialog = new Dialog(HomeDetailActivity.this, R.style.BottomDialogs);
        View linearView = LayoutInflater.from(HomeDetailActivity.this).inflate(R.layout.linearlayout_vertical, null);
        LinearLayout parentLinear = linearView.findViewById(R.id.linearlayout);


        parentLinear.addView(emptyView(""));
        bottomSheetDialog.setContentView(linearView);

        bottomSheetDialog.setCancelable(true);
        if (bottomSheetDialog.getWindow() != null) {
            bottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            bottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
            bottomSheetDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        bottomSheetDialog.show();

    }

    private View emptyView(String title) {
        View view1 = LayoutInflater.from(this).inflate(R.layout.single_text_item, null);
        TextView textView = view1.findViewById(R.id.tv_title);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(layoutParams);
        textView.setText("No Data Found");
        textView.setGravity(Gravity.CENTER);

        return view1;
    }

    private void noDataFound() {
        TextView textView = new TextView(this);
        textView.setText("No Data Found");
        textView.setTextColor(getResources().getColor(R.color.black_p50));
        parentLayout.addView(textView);
    }

}
