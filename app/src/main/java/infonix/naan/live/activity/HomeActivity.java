package infonix.naan.live.activity;

import android.Manifest;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import butterknife.BindView;
import butterknife.ButterKnife;
import infonix.naan.live.MainApplication;
import infonix.naan.live.R;
import infonix.naan.live.fragment.FirstStageHomeFragment;
import infonix.naan.live.listener.ObservableObject;
import infonix.naan.live.listener.SwipeListener;
import infonix.naan.live.network.ApiService;
import me.leolin.shortcutbadger.ShortcutBadger;

public class HomeActivity extends AppCompatActivity implements SwipeListener.SimpleGestureListener, Observer {

    private SwipeListener detector;
    private String title = "Home";

    private int selectedPos = -1;
    private int selectedColorCode = 0;

    private ApiService restService;
    private Bitmap icon;
    private Drawable wallpaperDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
        wallpaperDrawable = wallpaperManager.getDrawable();
        if (wallpaperDrawable != null) {
            icon = ((BitmapDrawable) wallpaperDrawable).getBitmap();
            int color = icon.getPixel(0, 0);
            if (color != 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                    Window window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(color);
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                }
            }
        }

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            SpannableString s = new SpannableString(title);
            s.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            Objects.requireNonNull(getSupportActionBar()).setTitle(s);
        }
*/
        ButterKnife.bind(this);
        restService = ((MainApplication)

                getApplication()).

                getClient();

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = new FirstStageHomeFragment();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.add(R.id.frame, fragment);
        fragmentTransaction.commit();

        ShortcutBadger.removeCount(this);

        detector = new

                SwipeListener(HomeActivity.this, this);
        ObservableObject.getInstance().

                addObserver(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onSwipe(int direction) {

        switch (direction) {

            case SwipeListener.SWIPE_RIGHT:
                break;
            case SwipeListener.SWIPE_LEFT:
                break;
            case SwipeListener.SWIPE_DOWN:
                break;
            case SwipeListener.SWIPE_UP:
                break;

        }
    }

    @Override
    public void onDoubleTap() {
        //Toast.makeText(this, "You have Double Tapped.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void update(Observable observable, Object o) {

    }


}
