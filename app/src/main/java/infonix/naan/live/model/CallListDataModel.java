package infonix.naan.live.model;

import java.util.ArrayList;

public class CallListDataModel {

    private ArrayList<CallListItemModel> listData;
    private ArrayList<CallListSortDataModel> sortData;

    public ArrayList<CallListItemModel> getListData() {
        return listData;
    }

    public void setListData(ArrayList<CallListItemModel> listData) {
        this.listData = listData;
    }

    public ArrayList<CallListSortDataModel> getSortData() {
        return sortData;
    }

    public void setSortData(ArrayList<CallListSortDataModel> sortData) {
        this.sortData = sortData;
    }
}
