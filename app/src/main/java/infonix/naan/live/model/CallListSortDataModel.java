package infonix.naan.live.model;

import java.util.ArrayList;

public class CallListSortDataModel {


    private String titleText;
    private ArrayList<String> titleData;


    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public ArrayList<String> getTitleData() {
        return titleData;
    }

    public void setTitleData(ArrayList<String> titleData) {
        this.titleData = titleData;
    }
}
