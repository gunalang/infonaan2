package infonix.naan.live.model;

public class CallListModel {

    private CallListDataModel data;

    public CallListDataModel getData() {
        return data;
    }

    public void setData(CallListDataModel data) {
        this.data = data;
    }
}
