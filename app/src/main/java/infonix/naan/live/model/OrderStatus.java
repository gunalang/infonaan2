package infonix.naan.live.model;


public enum OrderStatus {

    COMPLETED,
    ACTIVE,
    INACTIVE;

}
