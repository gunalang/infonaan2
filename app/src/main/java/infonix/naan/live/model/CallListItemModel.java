package infonix.naan.live.model;

import infonix.naan.live.R;

public class CallListItemModel {

    private String linkName;
    private String linkId;
    private String callPosition;
    private String nextCallDate;
    private String clientType;
    private String ageCount;
    private String html;
    private boolean shouldSticky;


    public CallListItemModel() {
    }


    public CallListItemModel(String linkName, String linkId, String callPosition, String nextCallDate,
                             String clientType, String ageCount, String html) {
        this.linkName = linkName;
        this.linkId = linkId;
        this.callPosition = callPosition;
        this.nextCallDate = nextCallDate;
        this.clientType = clientType;
        this.ageCount = ageCount;
        this.html = html;
    }


    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    public String getCallPosition() {
        return callPosition;
    }

    public void setCallPosition(String callPosition) {
        this.callPosition = callPosition;
    }

    public String getNextCallDate() {
        return nextCallDate;
    }

    public void setNextCallDate(String nextCallDate) {
        this.nextCallDate = nextCallDate;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getAgeCount() {
        return ageCount;
    }

    public void setAgeCount(String ageCount) {
        this.ageCount = ageCount;
    }


}
