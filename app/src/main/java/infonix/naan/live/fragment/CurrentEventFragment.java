package infonix.naan.live.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.JetPlayer;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;
import infonix.naan.live.R;
import infonix.naan.live.dbHelper.HelperCallRecordings;
import infonix.naan.live.listener.ObservableObject;
import infonix.naan.live.model.CallRecordListModel;
import infonix.naan.live.model.CallStats.CallStatsModel;
import infonix.naan.live.utils.DateTimeUtils;
import infonix.naan.live.utils.GlobalData;
import infonix.naan.live.view.fab.FloatingActionButton;
import infonix.naan.live.view.fab.FloatingActionMenu;
import infonix.naan.live.view.jcplayer.JcAudio;
import infonix.naan.live.view.jcplayer.JcPlayerView;

public class CurrentEventFragment extends Fragment implements Observer {

    @BindView(R.id.ll_view)
    LinearLayout currentViewLayout;

    private MediaPlayer mediaPlayer;

    private CallStatsModel callStatsModel;
    Handler handler1 = new Handler();
    private Cursor c;
    HelperCallRecordings hcr;
    SQLiteDatabase sdb;
    private ArrayList<CallRecordListModel> callListModelArrayList = new ArrayList<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_current_event, container, false);

        ButterKnife.bind(this, view);
        ObservableObject.getInstance().addObserver(this);
        callStatsModel = GlobalData.callStatsModel;

        getData();

        mediaPlayer = new MediaPlayer();
        return view;
    }

    private void getData() {
        if (getActivity() != null)
            if (callStatsModel != null)
                setData();
    }

    private void setData() {

        // callRecordView();
    }

    @Override
    public void onResume() {
        super.onResume();
        getCallData();
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o.toString().equals("1")) {
            getData();
        }
    }

    private void getCallData() {
        hcr = new HelperCallRecordings(getActivity());

        c = hcr.display();

        try {
            sdb = hcr.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Cursor cursor = sdb.rawQuery("select * from CallRecords", null);

        if (cursor.getCount() == 0) {
            Log.e("log", "err");
        } else {
            cursor.moveToFirst();
            callListModelArrayList.clear();
            do {
                CallRecordListModel note = new CallRecordListModel();
                note.setTime(cursor.getString(cursor.getColumnIndex("Time")));
                note.setNumber(cursor.getString(cursor.getColumnIndex("Number")));
                note.setCallType(cursor.getString(cursor.getColumnIndex("CallType")));
                note.setFilePath(cursor.getString(cursor.getColumnIndex("FilePath")));
                note.setStatus(cursor.getString(cursor.getColumnIndex("status")));

                StringTokenizer tk = new StringTokenizer(note.getTime());

                note.setTime(tk.nextToken() + " " + tk.nextToken());
                Uri uri = Uri.parse(note.getFilePath());
                int millSecond = 0;
                try {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(getActivity(), uri);
                    String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                    millSecond = Integer.parseInt(durationStr);
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
                note.setDuration(millSecond / (60 * 1000) + " min " + (millSecond / 1000) % (60) + " sec");
                callListModelArrayList.add(note);
            } while (cursor.moveToNext());
        }
        if (callListModelArrayList != null && callListModelArrayList.size() > 0) {
            callRecordView2(callListModelArrayList.get(callListModelArrayList.size() - 1).getFilePath(),
                    callListModelArrayList.get(callListModelArrayList.size() - 1).getTime(),
                    callListModelArrayList.get(callListModelArrayList.size() - 1).getNumber());
        }

    }

    private void callRecordView2(final String filepath, String date, String phone) {
        currentViewLayout.removeAllViews();
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.call_detail_item, currentViewLayout, false);

        TextView hotelName = view.findViewById(R.id.tv_hotelname);
        TextView callDate = view.findViewById(R.id.tv_calldate);
        TextView startTime = view.findViewById(R.id.tv_start_time);
        TextView endTime = view.findViewById(R.id.tv_end_time);
        JcPlayerView player = view.findViewById(R.id.jcplayer);
        EditText description = view.findViewById(R.id.et_description);
        Button send = view.findViewById(R.id.btn_send);
        callDate.setText(date);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        int millSecond = 0;
        File file = new File(filepath);
        try {
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(getActivity(), Uri.fromFile(file));
            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            millSecond = Integer.parseInt(durationStr);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy", Locale.ENGLISH);
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        Date dateFormate = null;
        Calendar calendar = null;
        try {
            dateFormate = sdf.parse(date);
            calendar = Calendar.getInstance();
            calendar.setTime(dateFormate);
            startTime.setText("Start Time : " + String.valueOf(sdf1.format(calendar.getTime())));
            calendar.add(Calendar.HOUR, (millSecond / (1000 * 60 * 60)));
            calendar.add(Calendar.MINUTE, (millSecond / (1000 * 60)));
            calendar.add(Calendar.SECOND, (millSecond / 1000) % 60);

            endTime.setText("End Time : " + String.valueOf(sdf1.format(calendar.getTime())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ArrayList<JcAudio> jcAudios = new ArrayList<>();
        jcAudios.add(JcAudio.createFromFilePath(phone, filepath));
        player.initPlaylist(jcAudios);

        if (callStatsModel != null && callStatsModel.getCompanyData() != null &&
                callStatsModel.getCompanyData().getClientName() != null)
            hotelName.setText(callStatsModel.getCompanyData().getClientName());

        currentViewLayout.addView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    @Override
    public void onPause() {
        if (mediaPlayer != null && mediaPlayer.isPlaying())
            mediaPlayer.stop();
        if (handler1 != null)
            handler1.removeCallbacksAndMessages(null);
        super.onPause();
    }
}
