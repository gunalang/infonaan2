package infonix.naan.live.fragment;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import butterknife.BindView;
import butterknife.ButterKnife;
import infonix.naan.live.R;
import infonix.naan.live.activity.ItemDetailActivity;
import infonix.naan.live.adapter.CallActivitiesAdapter;
import infonix.naan.live.adapter.CallPerformanceAdapter;
import infonix.naan.live.adapter.SingleTextAdapter;
import infonix.naan.live.listener.ObservableObject;
import infonix.naan.live.model.CallStats.CallStatsModel;
import infonix.naan.live.model.CallStats.DailyActivities;
import infonix.naan.live.model.OrderStatus;
import infonix.naan.live.model.TimeLineModel;
import infonix.naan.live.model.menu.BlockContent;
import infonix.naan.live.utils.GlobalData;


public class TimelineFragment extends Fragment implements Observer {

    @BindView(R.id.ll_parentlayout)
    LinearLayout parentLayout;

    private CallStatsModel callStatsModel;
    private boolean mWithLinePadding;
    private String datePattern = "([0-9]{2}) ([a-zA-Z]{3}) ([0-9]{4})";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline, container, false);

        ButterKnife.bind(this, view);
        ObservableObject.getInstance().addObserver(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        callStatsModel = GlobalData.callStatsModel;
        getData();

        return view;
    }

    private void setData() {
        if (callStatsModel != null) {
            doubleText();
            if(callStatsModel.getCallStatus().get(0).getCallPerformance() != null &&
                    callStatsModel.getCallStatus().get(0).getCallPerformance().size() > 0) {
                timeLineCallPerformanceView();
            }
            if(callStatsModel.getCallStatus().get(0).getDailyActivities() != null &&
                    callStatsModel.getCallStatus().get(0).getDailyActivities().size() > 0) {
                timeLineCallActivitiesView();
            }
        }
    }

    private void doubleText() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.linearlayout_vertical, null);

        LinearLayout linearLayout = view.findViewById(R.id.linearlayout);
        linearLayout.setBackgroundColor(Color.WHITE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            linearLayout.setElevation(5);
        }
        for (int i = 0; i < 3; i++) {
            View view1 = LayoutInflater.from(getActivity()).inflate(R.layout.double_text_horizontal_item, null);
            TextView heading = view1.findViewById(R.id.tv_title);
            TextView text = view1.findViewById(R.id.tv_title_sub);
            if (i == 0) {
                heading.setText("Next Appointment Date");
                if (callStatsModel.getCallStatus().get(0).getNextAppointmentDate() != null)
                    text.setText(callStatsModel.getCallStatus().get(0).getNextAppointmentDate());
            } else if (i == 1) {
                heading.setText("Age Count");
                if (callStatsModel.getCallStatus().get(0).getAgeNumber() != null)
                    text.setText(callStatsModel.getCallStatus().get(0).getAgeNumber());
            } else {
                heading.setText("Current Call Position");
                if (callStatsModel.getCallStatus().get(0).getCurrentCallPosition() != null)
                    text.setText(callStatsModel.getCallStatus().get(0).getCurrentCallPosition());
            }
            linearLayout.addView(view1, new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
        parentLayout.addView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void timeLineCallPerformanceView() {

        TextView textView = new TextView(getActivity());
        textView.setText("Call Performance");
        parentLayout.addView(textView, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.recyclerview, null);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        CallPerformanceAdapter callPerformanceAdapter = new CallPerformanceAdapter(
                callStatsModel.getCallStatus().get(0).getCallPerformance(), mWithLinePadding);
        recyclerView.setAdapter(callPerformanceAdapter);
        recyclerView.setBackgroundResource(R.drawable.border_bottom);

        parentLayout.addView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void timeLineCallActivitiesView() {

        TextView textView = new TextView(getActivity());
        textView.setText("Call Activities");
        parentLayout.addView(textView, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.recyclerview, null);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        Collections.sort(callStatsModel.getCallStatus().get(0).getDailyActivities(), new Comparator<DailyActivities>() {
            DateFormat f = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);

            @Override
            public int compare(DailyActivities lhs, DailyActivities rhs) {

                if (lhs.getCallDate().trim().matches(datePattern) &&
                        rhs.getCallDate().trim().matches(datePattern)) {
                    try {
                        return f.parse(lhs.getCallDate()).compareTo(f.parse(rhs.getCallDate()));
                    } catch (ParseException e) {
                        throw new IllegalArgumentException(e);
                    }
                } else {
                    return lhs.getCallDate().compareTo(rhs.getCallDate());
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        CallActivitiesAdapter callActivitiesAdapter = new CallActivitiesAdapter(
                callStatsModel.getCallStatus().get(0).getDailyActivities(), mWithLinePadding);
        recyclerView.setAdapter(callActivitiesAdapter);
        recyclerView.setBackgroundResource(R.drawable.border_bottom);

        parentLayout.addView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void getData() {
        if (getActivity() != null)
        if (callStatsModel != null)
            setData();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void update(Observable observable, Object o) {
        if (o.toString().equals("0")) {
            getData();
        }
    }
}
