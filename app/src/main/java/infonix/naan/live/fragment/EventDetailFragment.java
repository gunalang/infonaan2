package infonix.naan.live.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;
import infonix.naan.live.MainApplication;
import infonix.naan.live.R;
import infonix.naan.live.activity.ItemDetailActivity;
import infonix.naan.live.adapter.CallRecordAdapter;
import infonix.naan.live.adapter.DoubleTextAdapter;
import infonix.naan.live.adapter.SingleTextAdapter;
import infonix.naan.live.adapter.ExpandLayoutAdapter;
import infonix.naan.live.adapter.ViewSliderAdapter;
import infonix.naan.live.dbHelper.HelperCallRecordings;
import infonix.naan.live.listener.ObservableObject;
import infonix.naan.live.model.CallRecordListModel;
import infonix.naan.live.model.CallStats.CallStatsModel;
import infonix.naan.live.utils.GlobalData;
import infonix.naan.live.view.MovableFloatingActionButton;
import infonix.naan.live.view.fab.FloatingActionButton;
import infonix.naan.live.view.jcplayer.JcPlayerService;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static android.view.Gravity.BOTTOM;
import static android.view.Gravity.TOP;


public class EventDetailFragment extends Fragment implements Observer, CallRecordAdapter.AdapterInterface {

    @BindView(R.id.coordinator)
    CoordinatorLayout parentLayout;
    @BindView(R.id.rg_buttons)
    RadioGroup radioGroupButtons;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.horizontalscrollview)
    HorizontalScrollView horizontalScrollView;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;
    @BindView(R.id.fab_new)
    MovableFloatingActionButton fabNewTask;

    private AlertDialog alertDialog;
    private String[] list = new String[]{"call list", "History", "Add Appointment", "Contacts", "Tasks", "Notes"};

    private ArrayList<View> views;
    private CallStatsModel callStatsModel;
    private CallRecordAdapter callRecordAdapter;

    private Cursor c;
    HelperCallRecordings hcr;
    SQLiteDatabase sdb;
    LayoutInflater layoutInflater;
    CallRecordAdapter mAdapter;
    private ArrayList<CallRecordListModel> callListModelArrayList;
    boolean mBounded;
    private JcPlayerService jcPlayerService;
    private Calendar myCalendar;
    private SimpleDateFormat sdf;

    private long delayTime = 2000;
    private Handler handler = new Handler();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_detail, container, false);

        ButterKnife.bind(this, view);

        ObservableObject.getInstance().addObserver(this);
        callStatsModel = GlobalData.callStatsModel;
        myCalendar = Calendar.getInstance();
        sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        fabNewTask.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onClick(View view) {
                if (radioGroupButtons != null && radioGroupButtons.getChildCount() > 0) {
                    if (radioGroupButtons.getCheckedRadioButtonId() == 3) {
                        addNew("New Contact");
                    } else if (radioGroupButtons.getCheckedRadioButtonId() == 4) {
                        addNew("New Task");
                    } else if (radioGroupButtons.getCheckedRadioButtonId() == 5) {
                        addNew("New Notes");
                    }
                }
            }
        });
        if (ItemDetailActivity.colorCode != 0) {
            fabNewTask.setColorNormal(ItemDetailActivity.colorCode);
            fabNewTask.setColorPressed(ItemDetailActivity.colorCode);
        } else {
            fabNewTask.setColorNormal(R.color.colorPrimary);
            fabNewTask.setColorPressed(R.color.colorPrimary);
        }

        return view;
    }

    private void getData() {
        if (getActivity() != null)
            if (callStatsModel != null)
                setData();
    }

    private void setData() {

        if (callStatsModel.getAppointmentList() != null && callStatsModel.getAppointmentList().size() > 0) {
            addButtons();
        }

    }

    private void addButtons() {
        radioGroupButtons.removeAllViews();
        viewPager.removeAllViews();
        views = new ArrayList<View>();
        radioGroupButtons.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        for (int i = 0; i < list.length; i++) {
            RadioButton button = (RadioButton) LayoutInflater.from(getActivity()).inflate(R.layout.radiobutton, radioGroupButtons,
                    false);
            button.setText(list[i]);
            button.setId(i);

            if (list[i].equalsIgnoreCase("call list")) {
                views.add(getCallList());
                radioGroupButtons.addView(button);
            } else if (list[i].equalsIgnoreCase("History")) {
                views.add(expandableList());
                radioGroupButtons.addView(button);
            } else if (list[i].equalsIgnoreCase("Add Appointment")) {
                views.add(newAppointmnt());
                radioGroupButtons.addView(button);
            } else if (list[i].equalsIgnoreCase("Contacts")) {
                views.add(contacts());
                radioGroupButtons.addView(button);
            } else if (list[i].equalsIgnoreCase("Tasks")) {
                views.add(doubleList("Add New Task"));
                radioGroupButtons.addView(button);
            } else if (list[i].equalsIgnoreCase("Notes")) {
                views.add(emptyView("Add New Note"));
                radioGroupButtons.addView(button);
            } else {
                views.add(emptyView(""));
                radioGroupButtons.addView(button);
            }
        }
        final ViewSliderAdapter viewSliderAdapter = new ViewSliderAdapter(views);
        viewPager.setAdapter(viewSliderAdapter);
        viewPager.setOffscreenPageLimit(views.size());
        ((RadioButton) radioGroupButtons.getChildAt(0)).setChecked(true);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                if (radioGroupButtons.getChildCount() >= position) {
                    ((RadioButton) radioGroupButtons.getChildAt(position)).setChecked(true);
                }
                hideKeyboard();
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });

        radioGroupButtons.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                View tabView = radioGroup.getChildAt(i);
                if (i <= 2) {
                    fabNewTask.setVisibility(View.GONE);
                }
                if (i >= 3) {
                    fabNewTask.setVisibility(View.VISIBLE);
                }
                if (i < views.size()) {
                    viewPager.setCurrentItem(i);
                    int scrollPos = tabView.getLeft() - (horizontalScrollView.getWidth() - tabView.getWidth()) / 2;
                    horizontalScrollView.smoothScrollTo(scrollPos, 0);
                }
            }
        });
    }


    private View singleList() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.recyclerview, viewPager,
                false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        SingleTextAdapter adapter = new SingleTextAdapter(getActivity(), callStatsModel.getAppointmentList());
        recyclerView.setAdapter(adapter);
        recyclerView.setBackgroundResource(R.drawable.border_bottom);

        return view;
    }

    private View contacts() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.nestedscrollview, viewPager,
                false);
        NestedScrollView nestedScrollView = view.findViewById(R.id.nestedscrollview);
        View linearView = LayoutInflater.from(getActivity()).inflate(R.layout.linearlayout_vertical, viewPager,
                false);
        LinearLayout linearLayout = linearView.findViewById(R.id.linearlayout);
        View recycleView = LayoutInflater.from(getActivity()).inflate(R.layout.recyclerview, viewPager,
                false);
        RecyclerView recyclerView = recycleView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        SingleTextAdapter adapter = new SingleTextAdapter(getActivity(), callStatsModel.getAppointmentList());
        recyclerView.setAdapter(adapter);
       /* View buttonView = LayoutInflater.from(getActivity()).inflate(R.layout.button, viewPager,
                false);
        Button addNew = buttonView.findViewById(R.id.button1);
        addNew.setText("Add New Contact");
        addNew.setTextColor(getResources().getColor(android.R.color.black));
        addNew.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        addNew.setGravity(Gravity.CENTER);
        addNew.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            addNew.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.add_black_icon, 0);
        }
        addNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNew("New Contact");
            }
        });
*/
        linearLayout.setHorizontalGravity(Gravity.CENTER_HORIZONTAL);
        linearLayout.addView(recyclerView);
        // linearLayout.addView(buttonView);
        nestedScrollView.addView(linearLayout);

        return view;
    }


    private View emptyView(String title) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.linearlayout_vertical, viewPager,
                false);
        LinearLayout linearLayout = view.findViewById(R.id.linearlayout);

        View view1 = LayoutInflater.from(getActivity()).inflate(R.layout.single_text_item, viewPager,
                false);
        TextView textView = view1.findViewById(R.id.tv_title);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(layoutParams);
        textView.setText("No Data Found");
        textView.setGravity(Gravity.CENTER);

       /* View buttonView = LayoutInflater.from(getActivity()).inflate(R.layout.button, viewPager,
                false);
        Button addNew = buttonView.findViewById(R.id.button1);
        addNew.setText(title);
        addNew.setTextColor(getResources().getColor(android.R.color.black));
        addNew.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        addNew.setGravity(Gravity.CENTER);
        addNew.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            addNew.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.add_black_icon, 0);
        }
        addNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addNew("New Notes");
            }
        });*/

        linearLayout.setHorizontalGravity(Gravity.CENTER_HORIZONTAL);
        linearLayout.addView(view1);
        // if (!title.isEmpty())
        //   linearLayout.addView(buttonView);

        return view;
    }

    private View newAppointmnt() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.nestedscrollview, viewPager,
                false);
        NestedScrollView scrollView = view.findViewById(R.id.nestedscrollview);
        View linearLayout = LayoutInflater.from(getActivity()).inflate(R.layout.add_appintment_layout, viewPager,
                false);
        final TextView nextDate = linearLayout.findViewById(R.id.tv_nextdate);
        final TextView approxclosure = linearLayout.findViewById(R.id.tv_approxclosure);
        nextDate.setText(sdf.format(myCalendar.getTime()));
        approxclosure.setText(sdf.format(myCalendar.getTime()));

        nextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (getActivity() != null) {
                    DatePickerDialog datePickerDialogFrom = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                    nextDate.setText(sdf.format(myCalendar.getTime()));
                                }
                            }, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    datePickerDialogFrom.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    datePickerDialogFrom.show();
                }

            }
        });

        approxclosure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (getActivity() != null) {
                    DatePickerDialog datePickerDialogFrom = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                    approxclosure.setText(sdf.format(myCalendar.getTime()));
                                }
                            }, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    datePickerDialogFrom.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    datePickerDialogFrom.show();
                }

            }
        });

        Spinner level = linearLayout.findViewById(R.id.sp_level);
        ArrayList<String> levelItems = new ArrayList<>();
        levelItems.add("Prosecting");
        levelItems.add("Opportunity");
        levelItems.add("Qualifiying");
        levelItems.add("Lead");
        levelItems.add("Proposal");
        levelItems.add("Closure");
        levelItems.add("Sign-off");
        if (getActivity() != null) {
            ArrayAdapter<String> levelAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_dropdown_item_1line, levelItems);
            level.setAdapter(levelAdapter);
        }
        Spinner opinion = linearLayout.findViewById(R.id.sp_opinion);
        ArrayList<String> opinionItems = new ArrayList<>();
        opinionItems.add("Hot");
        opinionItems.add("Warm");
        opinionItems.add("Cold");
        opinionItems.add("Negative");
        opinionItems.add("Hold");
        if (getActivity() != null) {
            ArrayAdapter<String> opinionAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_dropdown_item_1line, opinionItems);
            opinion.setAdapter(opinionAdapter);
        }
        Spinner callquality = linearLayout.findViewById(R.id.sp_callquality);
        ArrayList<String> callqualityItems = new ArrayList<>();
        callqualityItems.add("Better");
        callqualityItems.add("PassOn");
        if (getActivity() != null) {
            ArrayAdapter<String> callqualityAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_dropdown_item_1line, callqualityItems);
            callquality.setAdapter(callqualityAdapter);
        }
        Spinner criticalflag = linearLayout.findViewById(R.id.sp_criticalflag);
        ArrayList<String> criticalflagItems = new ArrayList<>();
        criticalflagItems.add("No");
        criticalflagItems.add("Medium");
        criticalflagItems.add("High");
        if (getActivity() != null) {
            ArrayAdapter<String> criticalflagAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_dropdown_item_1line, criticalflagItems);
            criticalflag.setAdapter(criticalflagAdapter);
        }
        Spinner packageName = linearLayout.findViewById(R.id.sp_package);
        ArrayList<String> packageNameItems = new ArrayList<>();
        packageNameItems.add("studio: 22220 + 18%");
        packageNameItems.add("studio: 11110 + 18%");
        packageNameItems.add("Free");
        if (getActivity() != null) {
            ArrayAdapter<String> packageNameAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_dropdown_item_1line, packageNameItems);
            packageName.setAdapter(packageNameAdapter);
        }
        Spinner attendby = linearLayout.findViewById(R.id.sp_attendby);
        Spinner assigned_to = linearLayout.findViewById(R.id.sp_assigned_to);
        ArrayList<String> attendbyItems = new ArrayList<>();
        attendbyItems.add("Ignatious");
        attendbyItems.add("Abhi");
        attendbyItems.add("Anu");
        if (getActivity() != null) {
            ArrayAdapter<String> attendbyAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_dropdown_item_1line, attendbyItems);
            attendby.setAdapter(attendbyAdapter);
            assigned_to.setAdapter(attendbyAdapter);
        }
        Spinner spokento = linearLayout.findViewById(R.id.sp_spokento);
        Spinner futureConatct = linearLayout.findViewById(R.id.sp_future_conatct);
        ArrayList<String> spokentoItems = new ArrayList<>();
        spokentoItems.add("Rajesh");
        spokentoItems.add("Selva");
        spokentoItems.add("NA");
        if (getActivity() != null) {
            ArrayAdapter<String> spokentoAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_dropdown_item_1line, spokentoItems);
            spokento.setAdapter(spokentoAdapter);
            futureConatct.setAdapter(spokentoAdapter);
        }

        EditText desription = linearLayout.findViewById(R.id.et_desription);
        EditText nextAppointmentPurpose = linearLayout.findViewById(R.id.et_next_appointment_purpose);
        EditText internalNotes = linearLayout.findViewById(R.id.et_internal_notes);
        Button submit = linearLayout.findViewById(R.id.submit);

        scrollView.addView(linearLayout, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        return view;
    }

    private View doubleList(String title) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.nestedscrollview, viewPager,
                false);
        NestedScrollView nestedScrollView = view.findViewById(R.id.nestedscrollview);
        View linearView = LayoutInflater.from(getActivity()).inflate(R.layout.linearlayout_vertical, viewPager,
                false);
        LinearLayout linearLayout = linearView.findViewById(R.id.linearlayout);

        View view1 = LayoutInflater.from(getActivity()).inflate(R.layout.recyclerview, viewPager,
                false);
        RecyclerView recyclerView = view1.findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        DoubleTextAdapter adapter = new DoubleTextAdapter(getActivity(), callStatsModel.getAppointmentList());
        recyclerView.setAdapter(adapter);
        recyclerView.setPadding(0, 10, 0, 10);

      /*  View buttonView = LayoutInflater.from(getActivity()).inflate(R.layout.button, viewPager,
                false);
        Button addNew = buttonView.findViewById(R.id.button1);
        addNew.setText(title);
        addNew.setTextColor(getResources().getColor(android.R.color.black));
        addNew.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        addNew.setGravity(Gravity.CENTER);
        addNew.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            addNew.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.add_black_icon, 0);
        }
        addNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNew("New Task");
            }
        });
*/
        linearLayout.addView(view1);
        // linearLayout.addView(buttonView);

        nestedScrollView.addView(linearView);

        return view;
    }

    private View expandableList() {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.nestedscrollview, viewPager,
                false);
        NestedScrollView nestedScrollView = view.findViewById(R.id.nestedscrollview);
        View view1 = LayoutInflater.from(getActivity()).inflate(R.layout.recyclerview, viewPager,
                false);
        RecyclerView recyclerView = view1.findViewById(R.id.recyclerView);

        ExpandLayoutAdapter adapter = new ExpandLayoutAdapter(getActivity(),
                callStatsModel.getAppointmentList(), recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        nestedScrollView.addView(view1, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        /*parentLayout.addView(view, pos, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));*/
        return view;
    }

    private View getCallList() {
        hcr = new HelperCallRecordings(getActivity());
        c = hcr.display();

        try {
            sdb = hcr.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        @SuppressLint("Recycle") Cursor cursor = sdb.rawQuery("select * from CallRecords", null);

        if (cursor.getCount() == 0) {
            Log.e("log", "err");
            emptyView("");
        } else {
            cursor.moveToFirst();
            callListModelArrayList = new ArrayList<CallRecordListModel>();
            do {
                CallRecordListModel note = new CallRecordListModel();
                note.setTime(cursor.getString(cursor.getColumnIndex("Time")));
                note.setNumber(cursor.getString(cursor.getColumnIndex("Number")));
                note.setCallType(cursor.getString(cursor.getColumnIndex("CallType")));
                note.setFilePath(cursor.getString(cursor.getColumnIndex("FilePath")));
                note.setStatus(cursor.getString(cursor.getColumnIndex("status")));

                StringTokenizer tk = new StringTokenizer(note.getTime());

                note.setTime(tk.nextToken() + " " + tk.nextToken());
                Uri uri = Uri.parse(note.getFilePath());
                int millSecond = 0;
                try {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(getActivity(), uri);
                    String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                    millSecond = Integer.parseInt(durationStr);
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
                note.setDuration(millSecond / (60 * 1000) + " min " + (millSecond / 1000) % (60) + " sec");
                callListModelArrayList.add(note);
            } while (cursor.moveToNext());
        }

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.nestedscrollview, viewPager,
                false);
        NestedScrollView nestedScrollView = view.findViewById(R.id.nestedscrollview);
        View view1 = LayoutInflater.from(getActivity()).inflate(R.layout.recyclerview, viewPager,
                false);
        if (callListModelArrayList != null && callListModelArrayList.size() > 0) {
            Collections.reverse(callListModelArrayList);
            RecyclerView recyclerView = view1.findViewById(R.id.recyclerView);

            if (callListModelArrayList.size() > 0) {
                callRecordAdapter = new CallRecordAdapter(layoutInflater, callListModelArrayList,
                        getActivity(), this,
                        recyclerView);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(callRecordAdapter);

                nestedScrollView.addView(view1, new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
        }
        return view;
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o.toString().equals("2")) {
            getData();
        }
        if (o.toString().equals("refreshCallList")) {
            if (callRecordAdapter != null) {
                callRecordAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void deleteandset(int position) {
        c.moveToPosition(position);
        File file = new File(c.getString(2));

        HelperCallRecordings hcr = new HelperCallRecordings(MainApplication.getInstance());

        hcr.clearData(c.getString(c.getColumnIndex("Time")));

        c = hcr.display();

        if (c.getCount() == 0) {
            Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
        }
        callListModelArrayList.remove(position);
        callRecordAdapter.notifyItemRemoved(position);
        hcr.closeDatabase();

        if (file.exists()) {
            file.delete();
            Toast.makeText(MainApplication.getInstance(), "File deleted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainApplication.getInstance(), "File does not exist", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void addNew(String validation) {
        if (getActivity() != null) {
            final Dialog bottomSheetDialog = new Dialog(getActivity(), R.style.BottomDialogs);
            View view1 = LayoutInflater.from(getActivity()).inflate(R.layout.new_task_layout, null);

            LinearLayout linearLayout = view1.findViewById(R.id.ll_childview);
            LinearLayout frame = view1.findViewById(R.id.ll_frame);
            ScrollView scrollView = view1.findViewById(R.id.scrollView);
            CoordinatorLayout coordinatorLayout = view1.findViewById(R.id.ll_new);

            ImageView close = view1.findViewById(R.id.iv_close);
            TextView title = view1.findViewById(R.id.tv_title);
            title.setText(validation);
            if (validation.equalsIgnoreCase("New Contact")) {
                for (int i = 0; i < 5; i++) {
                    View spinnerView = LayoutInflater.from(getActivity()).inflate(R.layout.spinner, linearLayout, false);
                    Spinner spinner = spinnerView.findViewById(R.id.spinner);
                    View textViewView = LayoutInflater.from(getActivity()).inflate(R.layout.textview, linearLayout, false);
                    TextView textView = textViewView.findViewById(R.id.tv_text);
                    textView.setTextColor(getResources().getColor(R.color.editboxheading));
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_small));
                    View editView = LayoutInflater.from(getActivity()).inflate(R.layout.edittext, linearLayout, false);
                    EditText editText = editView.findViewById(R.id.edittext);
                    editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                //got focus
                                if (bottomSheetDialog.getWindow() != null)
                                    bottomSheetDialog.getWindow().setGravity(TOP);
                            } else {
                                //lost focus
                                if (bottomSheetDialog.getWindow() != null)
                                    bottomSheetDialog.getWindow().setGravity(BOTTOM);
                            }
                        }
                    });
                    if (i == 4) {
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
                        lp.setMargins(6, 0, 6, 0);
                        editText.setLayoutParams(lp);
                        editText.setGravity(Gravity.START);
                        editText.setSingleLine(false);

                        editText.setOnTouchListener(new View.OnTouchListener() {
                            public boolean onTouch(View v, MotionEvent event) {
                                v.getParent().requestDisallowInterceptTouchEvent(true);
                                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                                    case MotionEvent.ACTION_UP:
                                        v.getParent().requestDisallowInterceptTouchEvent(false);
                                        break;
                                }
                                return false;
                            }
                        });
                    }
                    if (i == 0)
                        textView.setText("Name");
                    else if (i == 1)
                        textView.setText("Designation");
                    else if (i == 2)
                        textView.setText("Phone");
                    else if (i == 3)
                        textView.setText("Email");
                    else
                        textView.setText("Comments");

                    linearLayout.addView(textViewView);
                    linearLayout.addView(editText);
                }
            } else if (validation.equalsIgnoreCase("new Task")) {
                for (int i = 0; i < 5; i++) {
                    View spinnerView = LayoutInflater.from(getActivity()).inflate(R.layout.spinner, linearLayout, false);
                    Spinner spinner = spinnerView.findViewById(R.id.spinner);
                    View textViewView = LayoutInflater.from(getActivity()).inflate(R.layout.textview, linearLayout, false);
                    TextView textView = textViewView.findViewById(R.id.tv_text);
                    textView.setTextColor(getResources().getColor(R.color.editboxheading));
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_small));
                    View editView = LayoutInflater.from(getActivity()).inflate(R.layout.edittext, linearLayout, false);
                    EditText editText = editView.findViewById(R.id.edittext);
                    editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                //got focus
                                if (bottomSheetDialog.getWindow() != null)
                                    bottomSheetDialog.getWindow().setGravity(TOP);
                            } else {
                                //lost focus
                                if (bottomSheetDialog.getWindow() != null)
                                    bottomSheetDialog.getWindow().setGravity(BOTTOM);
                            }
                        }
                    });
                    View dateView = LayoutInflater.from(getActivity()).inflate(R.layout.datepicker, linearLayout, false);
                    final TextView datePicker = dateView.findViewById(R.id.tv_date);
                    datePicker.setText(sdf.format(myCalendar.getTime()));
                    datePicker.setGravity(Gravity.START);
                    datePicker.setPadding(10, 0, 0, 0);

                    datePicker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (getActivity() != null) {
                                DatePickerDialog datePickerDialogFrom = new DatePickerDialog(getActivity(),
                                        new DatePickerDialog.OnDateSetListener() {
                                            @Override
                                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                                  int dayOfMonth) {
                                                myCalendar.set(Calendar.YEAR, year);
                                                myCalendar.set(Calendar.MONTH, monthOfYear);
                                                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                                datePicker.setText(sdf.format(myCalendar.getTime()));
                                            }
                                        }, myCalendar
                                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                        myCalendar.get(Calendar.DAY_OF_MONTH));
                                //datePickerDialogFrom.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                                datePickerDialogFrom.show();
                            }
                        }
                    });
                    if (i == 0)
                        textView.setText("Assigned By");
                    else if (i == 1)
                        textView.setText("Assigned To");
                    else if (i == 2)
                        textView.setText("Action");
                    else if (i == 3)
                        textView.setText("Complete By");
                    else
                        textView.setText("Comments");

                    if (i == 4) {
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
                        lp.setMargins(6, 0, 6, 0);
                        editText.setLayoutParams(lp);
                        editText.setGravity(Gravity.START);
                        editText.setSingleLine(false);

                        editText.setOnTouchListener(new View.OnTouchListener() {
                            public boolean onTouch(View v, MotionEvent event) {
                                v.getParent().requestDisallowInterceptTouchEvent(true);
                                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                                    case MotionEvent.ACTION_UP:
                                        v.getParent().requestDisallowInterceptTouchEvent(false);
                                        break;
                                }
                                return false;
                            }
                        });
                    }

                    ArrayList<String> attendbyItems = new ArrayList<>();
                    attendbyItems.add("Ignatious");
                    attendbyItems.add("Abhi");
                    attendbyItems.add("Anu");
                    ArrayList<String> actionItems = new ArrayList<>();
                    actionItems.add("Send Proposal");
                    actionItems.add("Demo");
                    actionItems.add("Direct Visit");
                    if (getActivity() != null) {
                        ArrayAdapter<String> attendbyAdapter = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_dropdown_item_1line, attendbyItems);
                        ArrayAdapter<String> actionAdapter = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_dropdown_item_1line, actionItems);
                        if (i <= 1)
                            spinner.setAdapter(attendbyAdapter);
                        else
                            spinner.setAdapter(actionAdapter);
                    }
                    linearLayout.addView(textViewView);
                    if (i <= 2)
                        linearLayout.addView(spinnerView);
                    else if (i == 3)
                        linearLayout.addView(dateView);
                    else
                        linearLayout.addView(editView);
                }
            } else {
                for (int i = 0; i < 3; i++) {
                    View spinnerView = LayoutInflater.from(getActivity()).inflate(R.layout.spinner, linearLayout, false);
                    Spinner spinner = spinnerView.findViewById(R.id.spinner);
                    View textViewView = LayoutInflater.from(getActivity()).inflate(R.layout.textview, linearLayout, false);
                    TextView textView = textViewView.findViewById(R.id.tv_text);
                    textView.setTextColor(getResources().getColor(R.color.editboxheading));
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_small));
                    View editView = LayoutInflater.from(getActivity()).inflate(R.layout.edittext, linearLayout, false);
                    EditText editText = editView.findViewById(R.id.edittext);
                    editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                //got focus
                                if (bottomSheetDialog.getWindow() != null)
                                    bottomSheetDialog.getWindow().setGravity(TOP);
                            } else {
                                //lost focus
                                if (bottomSheetDialog.getWindow() != null)
                                    bottomSheetDialog.getWindow().setGravity(BOTTOM);
                            }
                        }
                    });
                    if (i == 0)
                        textView.setText("Notes By");
                    else if (i == 1)
                        textView.setText("Flag");
                    else
                        textView.setText("Details");

                    ArrayList<String> attendbyItems = new ArrayList<>();
                    attendbyItems.add("Ignatious");
                    attendbyItems.add("Abhi");
                    attendbyItems.add("Anu");
                    ArrayList<String> flagItems = new ArrayList<>();
                    flagItems.add("White");
                    flagItems.add("Yellow");
                    flagItems.add("Red");
                    if (getActivity() != null) {
                        ArrayAdapter<String> attendbyAdapter = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_dropdown_item_1line, attendbyItems);
                        ArrayAdapter<String> flagAdapter = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_dropdown_item_1line, flagItems);
                        if (i == 0)
                            spinner.setAdapter(attendbyAdapter);
                        else spinner.setAdapter(flagAdapter);
                    }
                    if (i == 2) {
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
                        lp.setMargins(6, 0, 6, 0);
                        editText.setLayoutParams(lp);
                        editText.setGravity(Gravity.START);
                        editText.setSingleLine(false);
                    }

                    linearLayout.addView(textViewView);
                    if (i <= 1)
                        linearLayout.addView(spinnerView);
                    else
                        linearLayout.addView(editText);
                }
            }
            View buttonView = LayoutInflater.from(getActivity()).inflate(R.layout.button, linearLayout, false);
            Button submit = buttonView.findViewById(R.id.button1);
            submit.setText("Submit");
            submit.setTextColor(Color.BLACK);
            submit.setBackgroundColor(Color.TRANSPARENT);

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    hideKeyboard();
                    bottomSheetDialog.dismiss();
                }
            });
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    hideKeyboard();
                    bottomSheetDialog.dismiss();
                }
            });
            linearLayout.addView(buttonView);
            bottomSheetDialog.setContentView(view1);

            bottomSheetDialog.setCancelable(true);
            if (bottomSheetDialog.getWindow() != null) {
                bottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                bottomSheetDialog.getWindow().setGravity(Gravity.TOP);
                //bottomSheetDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            }

            bottomSheetDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            bottomSheetDialog.show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        Intent mIntent = new Intent(getActivity(), JcPlayerService.class);
        if (getActivity() != null)
            getActivity().bindService(mIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            //Service is disconnected
            mBounded = false;
            jcPlayerService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            //Service is connected
            mBounded = true;
            JcPlayerService.JcPlayerServiceBinder binder = (JcPlayerService.JcPlayerServiceBinder) service;
            jcPlayerService = binder.getService();
        }
    };

    @Override
    public void onStop() {
        if (mBounded) {
            if (getActivity() != null)
                getActivity().unbindService(mConnection);
            mBounded = false;
        }
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                getData();
                progressBar.setVisibility(View.GONE);
            }
        }, delayTime);
    }

    @Override
    public void onPause() {
        if (jcPlayerService != null)
            jcPlayerService.destroy();
        handler.removeCallbacksAndMessages(null);
        super.onPause();
    }

    private void hideKeyboard() {
        if (getActivity() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            if (imm != null && getView() != null) {
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        }
    }

    private void showKeyboard(View view) {
        if (getActivity() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            view.requestFocus();
            if (inputMethodManager != null) {
                inputMethodManager.showSoftInput(view, 0);
            }
        }
    }

}
