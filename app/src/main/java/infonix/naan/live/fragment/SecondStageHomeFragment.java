package infonix.naan.live.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import infonix.naan.live.MainApplication;
import infonix.naan.live.R;
import infonix.naan.live.activity.HomeDetailActivity;
import infonix.naan.live.adapter.MenuAdapter;
import infonix.naan.live.listener.ObservableObject;
import infonix.naan.live.model.SubMenuItem;
import infonix.naan.live.model.menu.ArgData;
import infonix.naan.live.model.menu.ChildBlockData;
import infonix.naan.live.model.menu.MenuDataModel;
import infonix.naan.live.network.ApiService;
import infonix.naan.live.utils.NetworkUtil;
import infonix.naan.live.view.GridSpacingItemDecoration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SecondStageHomeFragment extends Fragment implements MenuAdapter.HomeItemClickEventListener {

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.ll_parentview)
    LinearLayout parentLayout;
    @BindView(R.id.iv_close)
    ImageView close;
    @BindView(R.id.tv_header)
    TextView header;

    private Call<MenuDataModel> apiCall;
    private ApiService restService;
    private MenuDataModel menuDataModel = new MenuDataModel();
    private ArrayList<SubMenuItem> menuModelArrayList = new ArrayList<>();

    private int selectedPos = -1;
    private int selectedColorCode = 0;
    private String layoutId;
    private String title;
    private String url;
    private String method;
    private String params;
    private ArrayList<ArgData> argData;
    private Gson gson;
    private HashMap<String, String> paramsList = new HashMap<>();
    private int width, height;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_secondstage, container, false);

        ButterKnife.bind(this, view);
        gson = new Gson();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        Bundle bundle = getArguments();
        if (bundle != null) {
            layoutId = bundle.getString("linked");
            title = bundle.getString("title");
            url = bundle.getString("url");
            method = bundle.getString("method");
            params = bundle.getString("params");
            header.setText(title);
            argData = gson.fromJson(params, new TypeToken<ArrayList<ArgData>>() {
            }.getType());
            if (argData != null && argData.size() > 0) {
                for (ArgData argData : argData) {
                    paramsList.put(argData.getFldName(), argData.getFldValue());
                }
            }
        }

        if (getActivity() != null)
            restService = ((MainApplication) getActivity().getApplication()).getClient();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null)
                    getActivity().onBackPressed();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (new NetworkUtil().isNetworkAvailable(getActivity())) {
                    if (url != null && method != null && restService != null) {
                        getHomeListTask();
                    } else
                        Toast.makeText(getActivity(), "No url/method connection", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        try {
            swipeRefreshLayout.setColorSchemeResources((android.R.color.black),
                    (android.R.color.holo_green_light),
                    (android.R.color.holo_orange_light),
                    (android.R.color.holo_red_light));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (new NetworkUtil().isNetworkAvailable(getActivity())) {
            if (url != null && method != null && restService != null) {
                getHomeListTask();
            } else
                Toast.makeText(getActivity(), "No url/method connection", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    @Override
    public void homeItemClickEvent(int position, int colorCode, int childBlockosition, String layout, String count) {

        selectedPos = position;
        selectedColorCode = colorCode;
        if (getActivity() != null) {
            if (menuDataModel != null && menuDataModel.getData() != null) {
                if (menuDataModel.getData().get(0).getNaanMasterPage()
                        .getChildBlockData().get(childBlockosition).getBlockContent().get(position)
                        .getLinkSettings().getTargetLocationId() != null &&
                        menuDataModel.getData().get(0).getNaanMasterPage()
                                .getChildBlockData().get(childBlockosition).getBlockContent().get(position)
                                .getLinkSettings().getTargetLocationId().equalsIgnoreCase("0")) {
                    if (layout != null && layout.equalsIgnoreCase("gridView1")) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        Bundle bundle = new Bundle();
                        bundle.putString("layoutId", menuDataModel.getData().get(0).getNaanMasterPage()
                                .getChildBlockData().get(childBlockosition).getBlockContent().get(position)
                                .getLinkSettings().getLayoutId());
                        bundle.putString("title", menuDataModel.getData().get(0).getNaanMasterPage()
                                .getChildBlockData().get(childBlockosition).getBlockContent().get(position)
                                .getLinkSettings().getScreenName());
                        if (count != null && !count.isEmpty())
                            bundle.putString("sub_title", "(" + count + ")");
                        bundle.putString("url", menuDataModel.getData().get(0).getNaanMasterPage()
                                .getChildBlockData().get(childBlockosition).getBlockContent().get(position)
                                .getLinkSettings().getFetchURL());
                        bundle.putString("method", menuDataModel.getData().get(0).getNaanMasterPage()
                                .getChildBlockData().get(childBlockosition).getBlockContent().get(position)
                                .getLinkSettings().getArgType());
                        Gson gson = new Gson();
                        JsonElement jsonElement = gson.toJsonTree(menuDataModel.getData().get(0).getNaanMasterPage()
                                        .getChildBlockData().get(childBlockosition).getBlockContent().get(position)
                                        .getLinkSettings().getArgData(),
                                new TypeToken<ArrayList<ArgData>>() {
                                }.getType());
                        bundle.putString("params", jsonElement.toString());
                        Fragment fragment = new SecondStageHomeFragment();
                        fragment.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.add(R.id.frame, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    } else {
                        Intent bundle = new Intent(getActivity(), HomeDetailActivity.class);
                        bundle.putExtra("layoutId", menuDataModel.getData().get(0).getNaanMasterPage()
                                .getChildBlockData().get(childBlockosition).getBlockContent().get(position)
                                .getLinkSettings().getLayoutId());
                        if (count != null && !count.isEmpty())
                            bundle.putExtra("sub_title", "(" + count + ")");
                        bundle.putExtra("title", menuDataModel.getData().get(0).getNaanMasterPage()
                                .getChildBlockData().get(childBlockosition).getBlockContent().get(position)
                                .getLinkSettings().getScreenName());
                        bundle.putExtra("color", colorCode);
                        bundle.putExtra("url", menuDataModel.getData().get(0).getNaanMasterPage()
                                .getChildBlockData().get(childBlockosition).getBlockContent().get(position)
                                .getLinkSettings().getFetchURL());
                        bundle.putExtra("method", menuDataModel.getData().get(0).getNaanMasterPage()
                                .getChildBlockData().get(childBlockosition).getBlockContent().get(position)
                                .getLinkSettings().getArgType());
                        Gson gson = new Gson();
                        JsonElement jsonElement = gson.toJsonTree(menuDataModel.getData().get(0).getNaanMasterPage()
                                        .getChildBlockData().get(childBlockosition).getBlockContent().get(position)
                                        .getLinkSettings().getArgData(),
                                new TypeToken<ArrayList<ArgData>>() {
                                }.getType());
                        bundle.putExtra("params", jsonElement.toString());
                        startActivity(bundle);
                    }
                } else {
                    Toast.makeText(getActivity(), "target location here", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void getHomeListTask() {
        if (swipeRefreshLayout != null && !swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(true);
        }
        try {
            if (method.equalsIgnoreCase("POST")) {
                apiCall = restService.postMethodSubMenuList(url, paramsList);
            } else {
                StringBuilder p = new StringBuilder();
                p.setLength(0);
                for (int i = 0; i < argData.size(); i++) {
                    p.append("&").append(argData.get(i).getFldName()).append("=").append(argData.get(i).getFldValue());
                }
                apiCall = restService.getMethodSubMenuList(url + "?" + p.toString());
            }
            apiCall.enqueue(new Callback<MenuDataModel>() {
                @Override
                public void onResponse(@NonNull Call<MenuDataModel> call, @NonNull Response<MenuDataModel> response) {

                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    menuDataModel = response.body();
                    if (menuDataModel != null) {
                        parentLayout.removeAllViews();
                        for (int i = 0; i < menuDataModel.getData().size(); i++) {
                            for (int j = 0; j < menuDataModel.getData().get(i).getNaanMasterPage()
                                    .getChildBlockData().size(); j++) {
                                if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("transRecyclerView")) {
                                    verticalRecyclerView(menuDataModel.getData().get(i)
                                            .getNaanMasterPage().getChildBlockData().get(j), j);
                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("horizRecyclerView")) {
                                    horizontalRecyclerView(menuDataModel.getData().get(i)
                                            .getNaanMasterPage().getChildBlockData().get(j), j);
                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("gridRecyclerView")) {
                                    //gridview
                                    verticalRecyclerView(menuDataModel.getData().get(i)
                                            .getNaanMasterPage().getChildBlockData().get(j), j);
                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("listRecyclerView")) {
                                    //recyclerview
                                    verticalRecyclerView(menuDataModel.getData().get(i)
                                            .getNaanMasterPage().getChildBlockData().get(j), j);
                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("timelineRecyclerView")) {
                                    //timeline

                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("tabView")) {
                                    //tab view

                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("singleClmnTextBox")) {
                                    //single column textbox

                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("doubleClmnTextBox")) {
                                    //double column textbox

                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("fabButton")) {
                                    //fab buttons

                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("audioPlayBox")) {
                                    //audiobox

                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("audioList")) {
                                    //audio list

                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("expandableList")) {
                                    //expandable list

                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("singleClmnList")) {
                                    //single column list

                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("doubleClmnList")) {
                                    //double column list

                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("buttonList")) {
                                    //button list

                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("chatView")) {
                                    //chat view

                                } else if (menuDataModel.getData().get(i).getNaanMasterPage().getChildBlockData()
                                        .get(j).getBlockTemplateName().equalsIgnoreCase("bottomSheet")) {
                                    //call bottom sheet
                                }
                            }
                        }

                    } else {
                        Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<MenuDataModel> call, @NonNull Throwable t) {
                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    if (getActivity() != null && !t.getMessage().equalsIgnoreCase("Canceled"))
                        new android.support.v7.app.AlertDialog.Builder(getActivity())
                                .setMessage("Network Connection Failed")
                                .setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (url != null && method != null) {
                                            getHomeListTask();
                                        } else
                                            Toast.makeText(getActivity(), "No url/method connection", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                    }
                                })
                                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                }
            });

        } catch (Exception e) {
            if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
            e.printStackTrace();
        }
    }

    private void verticalRecyclerView(ChildBlockData childBlockData, int childBlockosition) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.recyclerview, parentLayout, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        int layoutWidth = 150;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        if (childBlockData.getBlockSettings() != null && childBlockData.getBlockSettings().size() > 0) {
            for (int i = 0; i < childBlockData.getBlockSettings().size(); i++) {
                if (childBlockData.getBlockSettings().get(i).getFldName().equalsIgnoreCase("colsInRow")) {
                    if (childBlockData.getBlockSettings().get(i).getFldValue() != null) {
                        recyclerView.addItemDecoration(new GridSpacingItemDecoration(
                                Integer.parseInt(childBlockData.getBlockSettings().get(i).getFldValue()), 2, true));
                        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),
                                Integer.parseInt(childBlockData.getBlockSettings().get(i).getFldValue())));
                    }
                } else if (childBlockData.getBlockSettings().get(i).getFldName().equalsIgnoreCase("blockWidth%")) {
                    layoutWidth = width / 100 * Integer.parseInt(childBlockData.getBlockSettings().get(i).getFldValue());
                }
            }
        }
        MenuAdapter adapter = new MenuAdapter(childBlockosition, getActivity(), childBlockData.getBlockContent(),
                SecondStageHomeFragment.this, true, layoutWidth);
        recyclerView.setAdapter(adapter);

        parentLayout.addView(view, 0, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    private void horizontalRecyclerView(ChildBlockData childBlockData, int childBlockosition) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.recyclerview, parentLayout, false);
        RecyclerView horizontalRecyclerview = view.findViewById(R.id.recyclerView);
        int layoutWidth = 150;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        horizontalRecyclerview.setLayoutManager(linearLayoutManager);
        horizontalRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false));
        if (childBlockData.getBlockSettings() != null && childBlockData.getBlockSettings().size() > 0) {
            for (int i = 0; i < childBlockData.getBlockSettings().size(); i++) {
                if (childBlockData.getBlockSettings().get(i).getFldName().equalsIgnoreCase("colsInRow")) {
                    if (childBlockData.getBlockSettings().get(i).getFldValue() != null) {
                        horizontalRecyclerview.addItemDecoration(new GridSpacingItemDecoration(
                                Integer.parseInt(childBlockData.getBlockSettings().get(i).getFldValue()), 2, true));
                        horizontalRecyclerview.setLayoutManager(new GridLayoutManager(getActivity(),
                                Integer.parseInt(childBlockData.getBlockSettings().get(i).getFldValue())));
                    }
                } else if (childBlockData.getBlockSettings().get(i).getFldName().equalsIgnoreCase("blockWidth%")) {
                    layoutWidth = width / 100 * Integer.parseInt(childBlockData.getBlockSettings().get(i).getFldValue());
                }
            }
        }
        MenuAdapter horizontalAdapter = new MenuAdapter(childBlockosition, getActivity(), childBlockData.getBlockContent(),
                SecondStageHomeFragment.this, false, layoutWidth);
        horizontalRecyclerview.setAdapter(horizontalAdapter);

        parentLayout.addView(view, 0, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    @Override
    public void onResume() {
        super.onResume();
        //ObservableObject.getInstance().updateValue("home_bottomsheet");
    }


    @Override
    public void onPause() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
        if (apiCall != null)
            apiCall.cancel();
        super.onPause();
    }

    private void bottomDialog() {
        if(getActivity() != null) {
            final Dialog bottomSheetDialog = new Dialog(getActivity(), R.style.BottomDialogs);
            View linearView = LayoutInflater.from(getActivity()).inflate(R.layout.linearlayout_vertical, null);
            LinearLayout parentLinear = linearView.findViewById(R.id.linearlayout);


            parentLinear.addView(emptyView(""));
            bottomSheetDialog.setContentView(linearView);

            bottomSheetDialog.setCancelable(true);
            if (bottomSheetDialog.getWindow() != null) {
                bottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                bottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
                bottomSheetDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            }
            bottomSheetDialog.show();
        }
    }

    private View emptyView(String title) {
        View view1 = LayoutInflater.from(getActivity()).inflate(R.layout.single_text_item, null);
        TextView textView = view1.findViewById(R.id.tv_title);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(layoutParams);
        textView.setText("No Data Found");
        textView.setGravity(Gravity.CENTER);

        return view1;
    }



}
