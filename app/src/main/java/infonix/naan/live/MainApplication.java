package infonix.naan.live;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import infonix.naan.live.network.ApiService;
import infonix.naan.live.service.CallRecorderServiceOptional;
import infonix.naan.live.utils.GlobalData;
import io.fabric.sdk.android.Fabric;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainApplication extends MultiDexApplication {

    Retrofit restClient;

    public static MainApplication mApplication;
    public static SharedPreferences sp;
    public static SharedPreferences.Editor e;

    public static MainApplication getInstance() {
        return mApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            Fabric.with(this, new Crashlytics());

            mApplication =this;
            sp=getApplicationContext().getSharedPreferences("com.infonaan", Context.MODE_PRIVATE);
            e=sp.edit();

            Gson gson = new GsonBuilder().setLenient().create();

            restClient = new Retrofit.Builder()
                    .baseUrl(GlobalData.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(new OkHttpClient.Builder()
                            .readTimeout(60, TimeUnit.SECONDS)
                            .connectTimeout(60, TimeUnit.SECONDS)
                            .build())
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {


            Intent opt = new Intent(this, CallRecorderServiceOptional.class);
           if (sp.getInt("type", 0) == 1) {
                stopService(opt);
            } else if (sp.getInt("type", 0) == 2) {
                startService(opt);
            }
        } catch (Exception e) {
            Log.e("application", "service");
        }
    }

    public ApiService getClient() {
        return restClient.create(ApiService.class);
    }

    public void resetService() {
        try {

            Intent opt = new Intent(this, CallRecorderServiceOptional.class);
            stopService(opt);
            if (sp.getInt("type", 0) == 1) {
                stopService(opt);
            } else if (sp.getInt("type", 0) == 2) {
                startService(opt);
            }
        } catch (Exception e) {
            Log.e("application", "reset service");
        }
    }

}
